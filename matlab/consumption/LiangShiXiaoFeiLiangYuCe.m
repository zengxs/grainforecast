%从后往前取数据，采用采用多元一次回归模型，区间归一化，增加新信息型，残差修正

function [X_Niandu_YuCe,Y_Shijixiaofeiliang_HuiSeYuCe,Y_Shijixiaofeiliang_DuoYuanYuCe,Y_Shijixiaofeiliang_DuoYuanYuCe_Xiuzheng,GuanLianDu, ...
    GuanLianXiShu,GuanLianXiShu_Name,GuanLianXiShu_YuCe,GuanLianXiShu_YuCe_Name] ...
    =LiangShiXiaoFeiLiangYuCe(X_Niandu,Y_Shijixiaofeiliang,Y_Shijixiaofeiliang_YuCe,Y_Shijixiaofeiliang_ChuShiXiang1,GuanLianDu,GuanLianXiShu,ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1, ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1,XiangCunRenKou_ChuShiXiang1,XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1,M,N,GuanLianDu_Yuzhi);


%多元回归
X1=[ones(M,1),ChengZhenRenKou_ChuShiXiang1(1:M),ChengZhenHuashuiping_ChuShiXiang1(1:M),ChengZhenEnGeErXiShu_ChuShiXiang1(1:M), ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1(1:M),ChengZhenRenJunShouRu_ChuShiXiang1(1:M), XiangCunRenKou_ChuShiXiang1(1:M),  XiangCunEnGeErXiShu_ChuShiXiang1(1:M), XiangCunRenJunShouRu_ChuShiXiang1(1:M)];
% 筛选影响因子，只保留关联度大的影子
XXX1=X1(:,1);
for i=1:size(GuanLianDu,2)
    if GuanLianDu(i)>GuanLianDu_Yuzhi
        XXX1=[XXX1,X1(:,i+1)];
    end
end
%筛选影响因子完毕
B1=regress(Y_Shijixiaofeiliang_ChuShiXiang1(1:M),XXX1);% 多元一次线性回归

%-------开始预测---------

X2=[ones(M+N,1),ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1,NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1, ...
     XiangCunRenKou_ChuShiXiang1,  XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1];
% 筛选影响因子，只保留关联度大的影子
XXX2=X2(:,1);
X3={'城镇人口','城镇化水平','城镇恩格尔系数','农产品价格指数','城镇人均收入','乡村人口','乡村恩格尔系数','乡村人均收入'};
X4=[];% 用于预测的影响因子关联系数
X5={};% 用于预测的影响因子名称
for i=1:size(GuanLianDu,2)
    if GuanLianDu(i)>GuanLianDu_Yuzhi
        XXX2=[XXX2,X2(:,i+1)];
        X4=[X4,GuanLianXiShu(:,i)];
        X5=[X5,X3{i}];
    end
end
GuanLianXiShu_Name=X3;
GuanLianXiShu_YuCe=X4;
GuanLianXiShu_YuCe_Name=X5;
%筛选影响因子完毕
Shijixiaofeiliang_HuiSeYuCe_Temp2=XXX2*B1;%多元一次线性回归预测产量


%一阶缓冲算子逆处理
Shijixiaofeiliang_HuiSeYuCe_Temp3(M+N,1)=Shijixiaofeiliang_HuiSeYuCe_Temp2(end,1);
for i=M+N-1:-1:1
    Shijixiaofeiliang_HuiSeYuCe_Temp3(i,1)=Shijixiaofeiliang_HuiSeYuCe_Temp2(i,1)*(M+N-i+1)-sum(Shijixiaofeiliang_HuiSeYuCe_Temp3(i+1:end));
end
%一阶缓冲算子处理完毕
%初始像逆运算
Shijixiaofeiliang_HuiSeYuCe=Shijixiaofeiliang_HuiSeYuCe_Temp3*(max(Y_Shijixiaofeiliang_YuCe)-min(Y_Shijixiaofeiliang_YuCe))+min(Y_Shijixiaofeiliang_YuCe);
%初始像逆运算完毕

%残差修正
Error_Juedui=abs(Y_Shijixiaofeiliang(1:M)-Shijixiaofeiliang_HuiSeYuCe(1:M));
M1=M;
for i=1:N
    [Error1]=HuiSeYuCe(Error_Juedui,M1); 
    Error_Juedui=[Error_Juedui;Error1];
    M1=M1+1;
end
Shijixiaofeiliang_HuiSeYuCe_Xiuzheng=Shijixiaofeiliang_HuiSeYuCe+sign(Y_Shijixiaofeiliang_YuCe-Shijixiaofeiliang_HuiSeYuCe).*Error_Juedui;

%残差修正结束
%----保存预测数据-----
for i=1:N
   X_Niandu_YuCe(i)=X_Niandu(end)+i;
end


Y_Shijixiaofeiliang_HuiSeYuCe=Y_Shijixiaofeiliang_YuCe(M+1:end)/1000;%----产量预测-----
Y_Shijixiaofeiliang_DuoYuanYuCe=Shijixiaofeiliang_HuiSeYuCe(M+1:end)/1000;
Y_Shijixiaofeiliang_DuoYuanYuCe_Xiuzheng=Shijixiaofeiliang_HuiSeYuCe_Xiuzheng(M+1:end)/1000;

