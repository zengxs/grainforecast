clear all;
clc;


load OriginalData.mat X_Niandu Y_liangshichanliang Y_chengzhenkouliangxiaofeiliang Y_chengzhensiliaoliangxiaofeiliang ...
    ChengZhenRenKou ChengZhenHuashuiping ChengZhenEnGeErXiShu NongChanPinShengChanJiaGeZhiShu ChengZhenRenJunShouRu Y_xiangcunkouliangxiaofeiliang ...
    Y_xiangcunsiliaoliangxiaofeiliang XiangCunRenKou XiangCunEnGeErXiShu XiangCunRenJunShouRu Y_liangshichanliang_HN Y_chengzhenkouliangxiaofeiliang_HN Y_chengzhensiliaoliangxiaofeiliang_HN ...
    ChengZhenRenKou_HN ChengZhenHuashuiping_HN ChengZhenEnGeErXiShu_HN NongChanPinShengChanJiaGeZhiShu_HN ChengZhenRenJunShouRu_HN...
    Y_xiangcunkouliangxiaofeiliang_HN Y_xiangcunsiliaoliangxiaofeiliang_HN XiangCunRenKou_HN XiangCunEnGeErXiShu_HN XiangCunRenJunShouRu_HN

% M 取数据的年数， N 预测年数，可以随意设置  N=5  中短期预测，N=10 长期预测
M=80;
N=10;  
GuanLianDu_Yuzhi=0.7;
% YuCeNeiRong:所需预测的内容：10:'全国粮食消费量'，包括：全国乡村口粮消费量','全国城镇口粮消费量','全国乡村饲料粮消费量','全国城镇饲料粮消费量';
%%%                          20:'河南粮食消费量'，包括： '河南乡村口粮消费量','河南城镇口粮消费量','河南乡村饲料粮消费量','河南城镇饲料粮消费量'.

 YuCeNeiRong=20;
 %全国：10
 %河南：20

[X_Niandu_YuCe,M,Y_liangshizongxiaofeiliang_HuiSeYuCe,Y_gongyeyongliang_HuiSeYuCe,Y_sunhao_HuiSeYuCe,Y_zhongzi_HuiSeYuCe,Y_kouliangzongxiaofeiliang_HuiSeYuCe,Y_siliaoliangzongxiaofeiliang_HuiSeYuCe,Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe,Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe,...
    Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe,Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe,GuanLianXiShu_1_YuCe,GuanLianXiShu_2_YuCe,GuanLianXiShu_3_YuCe,GuanLianXiShu_4_YuCe,GuanLianXiShu_1_YuCe_Name,GuanLianXiShu_2_YuCe_Name,GuanLianXiShu_3_YuCe_Name,GuanLianXiShu_4_YuCe_Name]...
     =liangshizongxiaofeiliangyuce(X_Niandu,GuanLianDu_Yuzhi,M,N,YuCeNeiRong,Y_liangshichanliang,Y_chengzhenkouliangxiaofeiliang,Y_chengzhensiliaoliangxiaofeiliang,ChengZhenRenKou,ChengZhenHuashuiping,ChengZhenEnGeErXiShu,NongChanPinShengChanJiaGeZhiShu,ChengZhenRenJunShouRu,Y_xiangcunkouliangxiaofeiliang,...
    Y_xiangcunsiliaoliangxiaofeiliang,XiangCunRenKou,XiangCunEnGeErXiShu,XiangCunRenJunShouRu,Y_liangshichanliang_HN,Y_chengzhenkouliangxiaofeiliang_HN,Y_chengzhensiliaoliangxiaofeiliang_HN, ...
    ChengZhenRenKou_HN,ChengZhenHuashuiping_HN,ChengZhenEnGeErXiShu_HN,NongChanPinShengChanJiaGeZhiShu_HN,ChengZhenRenJunShouRu_HN,Y_xiangcunkouliangxiaofeiliang_HN,Y_xiangcunsiliaoliangxiaofeiliang_HN,XiangCunRenKou_HN,XiangCunEnGeErXiShu_HN,XiangCunRenJunShouRu_HN);



figure(1);
bar(X_Niandu_YuCe,Y_liangshizongxiaofeiliang_HuiSeYuCe);
legend('联合动态预测粮食总消费量');
xlabel('年度');ylabel('粮食总消费量（万吨）');


figure(2);
bar(X_Niandu_YuCe,Y_kouliangzongxiaofeiliang_HuiSeYuCe);
xlabel('年度');ylabel('口粮总消费量（万吨）');

figure(3);
bar(X_Niandu_YuCe,Y_siliaoliangzongxiaofeiliang_HuiSeYuCe);
xlabel('年度');ylabel('饲料粮总消费量（万吨）');


figure(4)
subplot(2,1,1)
bar(X_Niandu_YuCe,Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe);
grid;
legend('城镇口粮消费量预测');
title('城镇口粮消费量预测值');
xlabel('年度');ylabel('城镇口粮消费量（万吨）');

subplot(2,1,2)
bar(X_Niandu_YuCe,GuanLianXiShu_1_YuCe(M+1:end,:));ylim([0 1.2])
grid;
legend(GuanLianXiShu_1_YuCe_Name);
title('城镇口粮消费量预测影响因子关联系数');
xlabel('年度');ylabel('关联系数');



figure(5)
subplot(2,1,1)
bar(X_Niandu_YuCe,Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe);
grid;
legend('城镇饲料粮消费量预测');% 
title('城镇饲料粮消费量预测值');
xlabel('年度');ylabel('城镇饲料粮消费量（万吨）');

subplot(2,1,2)
bar(X_Niandu_YuCe,GuanLianXiShu_2_YuCe(M+1:end,:));ylim([0 1.2])
grid;
legend(GuanLianXiShu_2_YuCe_Name);
title('城镇饲料粮消费量预测影响因子关联系数');
xlabel('年度');ylabel('关联系数');

figure(6)
subplot(2,1,1)
bar(X_Niandu_YuCe,Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe);
grid;
legend('乡村口粮消费量预测');% DI-AFS表示“差分及影响因子自动筛选”，DI-AFS-EM表示“差分及影响因子自动筛选及残差修正”
title('乡村口粮消费量预测值');
xlabel('年度');ylabel('乡村口粮消费量（万吨）');

subplot(2,1,2)
bar(X_Niandu_YuCe,GuanLianXiShu_3_YuCe(M+1:end,:));ylim([0 1.2])  
grid;
legend(GuanLianXiShu_3_YuCe_Name);
title(['乡村口粮消费量预测影响因子关联系数']);
xlabel('年度');ylabel('关联系数');

figure(7)
subplot(2,1,1)
bar(X_Niandu_YuCe,Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe);
grid;
legend('乡村饲料粮消费量预测');%
title(['乡村饲料粮消费量预测值']);
xlabel('年度');ylabel('乡村饲料粮消费量（万吨）');

subplot(2,1,2)
bar(X_Niandu_YuCe,GuanLianXiShu_4_YuCe(M+1:end,:));ylim([0 1.2])
grid;
legend(GuanLianXiShu_4_YuCe_Name);
title('乡村饲料粮消费量预测影响因子关联系数');
xlabel('年度');ylabel('关联系数');
