%   采用区间归一化
function [Export_Data]=HuiSeYuCe(Import_Data,M)
Import_Data_ChuShiXiang=(Import_Data-min(Import_Data))/(max(Import_Data)-min(Import_Data));%初始像
%数据预处理
Import_Data_ChuShiXiang1=exp(Import_Data_ChuShiXiang);
%产生累加生成列X1
X1=cumsum(Import_Data_ChuShiXiang1);
%构造数据矩阵B和数据向量Y
for j=2:M
    z(j,1)=(1/2).*(X1(j)+X1(j-1));
end
B=[-z(2:end),ones(M-1,1)];
Y=Import_Data_ChuShiXiang1(2:M);
%计算模型参数Alpha_temp=（Alpha Miu）'
Alpha_temp=inv(B'*B)*B'*Y;
%写出GM(1，1)预测模型X2
for j=1:M+1;
    X2(j,1)=(Import_Data_ChuShiXiang1(1)-Alpha_temp(2)/Alpha_temp(1))*exp(-Alpha_temp(1)*(j-1))+Alpha_temp(2)/Alpha_temp(1);
end
%还原累减生成趋势产量：Qushichanliang_HuiSeYuCe
Export_Data_Temp(1,1)=X1(1);
for j=2:M+1
    Export_Data_Temp(j,1)=X2(j)-X2(j-1);
end
%数据逆处理
Export_Data_Temp=log(Export_Data_Temp);
%由于取的是最后一个数据，可以不需要逆一阶算子还原，只需初始像逆运算
Export_Data=Export_Data_Temp(end,1)*(max(Import_Data)-min(Import_Data))+min(Import_Data);
