/*
 * MATLAB Compiler: 4.11 (R2009b)
 * Date: Wed Nov  1 00:10:40 2017
 * Arguments: "-B" "macro_default" "-B" "csharedlib:libconsumption" "-W"
 * "lib:libconsumption" "-T" "link:lib" "-d" "dist"
 * "liangshizongxiaofeiliangyuce.m" 
 */

#ifndef __libconsumption_h
#define __libconsumption_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_libconsumption
#define PUBLIC_libconsumption_C_API __global
#else
#define PUBLIC_libconsumption_C_API /* No import statement needed. */
#endif

#define LIB_libconsumption_C_API PUBLIC_libconsumption_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_libconsumption
#define PUBLIC_libconsumption_C_API __declspec(dllexport)
#else
#define PUBLIC_libconsumption_C_API __declspec(dllimport)
#endif

#define LIB_libconsumption_C_API PUBLIC_libconsumption_C_API


#else

#define LIB_libconsumption_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_libconsumption_C_API 
#define LIB_libconsumption_C_API /* No special import/export declaration */
#endif

extern LIB_libconsumption_C_API 
bool MW_CALL_CONV libconsumptionInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_libconsumption_C_API 
bool MW_CALL_CONV libconsumptionInitialize(void);

extern LIB_libconsumption_C_API 
void MW_CALL_CONV libconsumptionTerminate(void);



extern LIB_libconsumption_C_API 
void MW_CALL_CONV libconsumptionPrintStackTrace(void);

extern LIB_libconsumption_C_API 
bool MW_CALL_CONV mlxLiangshizongxiaofeiliangyuce(int nlhs, mxArray *plhs[], int nrhs, 
                                                  mxArray *prhs[]);

extern LIB_libconsumption_C_API 
long MW_CALL_CONV libconsumptionGetMcrID();



extern LIB_libconsumption_C_API bool MW_CALL_CONV mlfLiangshizongxiaofeiliangyuce(int nargout, mxArray** X_Niandu_YuCe, mxArray** M, mxArray** Y_liangshizongxiaofeiliang_HuiSeYuCe, mxArray** Y_gongyeyongliang_HuiSeYuCe, mxArray** Y_sunhao_HuiSeYuCe, mxArray** Y_zhongzi_HuiSeYuCe, mxArray** Y_kouliangzongxiaofeiliang_HuiSeYuCe, mxArray** Y_siliaoliangzongxiaofeiliang_HuiSeYuCe, mxArray** Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe, mxArray** Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe, mxArray** Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe, mxArray** Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe, mxArray** GuanLianXiShu_1_YuCe, mxArray** GuanLianXiShu_2_YuCe, mxArray** GuanLianXiShu_3_YuCe, mxArray** GuanLianXiShu_4_YuCe, mxArray** GuanLianXiShu_1_YuCe_Name, mxArray** GuanLianXiShu_2_YuCe_Name, mxArray** GuanLianXiShu_3_YuCe_Name, mxArray** GuanLianXiShu_4_YuCe_Name, mxArray* X_Niandu, mxArray* GuanLianDu_Yuzhi, mxArray* M_in1, mxArray* N, mxArray* YuCeNeiRong, mxArray* Y_liangshichanliang, mxArray* Y_chengzhenkouliangxiaofeiliang, mxArray* Y_chengzhensiliaoliangxiaofeiliang, mxArray* ChengZhenRenKou, mxArray* ChengZhenHuashuiping, mxArray* ChengZhenEnGeErXiShu, mxArray* NongChanPinShengChanJiaGeZhiShu, mxArray* ChengZhenRenJunShouRu, mxArray* Y_xiangcunkouliangxiaofeiliang, mxArray* Y_xiangcunsiliaoliangxiaofeiliang, mxArray* XiangCunRenKou, mxArray* XiangCunEnGeErXiShu, mxArray* XiangCunRenJunShouRu, mxArray* Y_liangshichanliang_HN, mxArray* Y_chengzhenkouliangxiaofeiliang_HN, mxArray* Y_chengzhensiliaoliangxiaofeiliang_HN, mxArray* ChengZhenRenKou_HN, mxArray* ChengZhenHuashuiping_HN, mxArray* ChengZhenEnGeErXiShu_HN, mxArray* NongChanPinShengChanJiaGeZhiShu_HN, mxArray* ChengZhenRenJunShouRu_HN, mxArray* Y_xiangcunkouliangxiaofeiliang_HN, mxArray* Y_xiangcunsiliaoliangxiaofeiliang_HN, mxArray* XiangCunRenKou_HN, mxArray* XiangCunEnGeErXiShu_HN, mxArray* XiangCunRenJunShouRu_HN);

#ifdef __cplusplus
}
#endif
#endif
