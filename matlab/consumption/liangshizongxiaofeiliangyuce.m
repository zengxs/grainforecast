%从后往前取数据，采用采用多元一次回归模型，区间归一化，增加新信息型，残差修正

function [X_Niandu_YuCe,M,Y_liangshizongxiaofeiliang_HuiSeYuCe,Y_gongyeyongliang_HuiSeYuCe,Y_sunhao_HuiSeYuCe,Y_zhongzi_HuiSeYuCe,Y_kouliangzongxiaofeiliang_HuiSeYuCe,Y_siliaoliangzongxiaofeiliang_HuiSeYuCe,Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe,Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe,...
    Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe,Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe,GuanLianXiShu_1_YuCe,GuanLianXiShu_2_YuCe,GuanLianXiShu_3_YuCe,GuanLianXiShu_4_YuCe,GuanLianXiShu_1_YuCe_Name,GuanLianXiShu_2_YuCe_Name,GuanLianXiShu_3_YuCe_Name,GuanLianXiShu_4_YuCe_Name]...
     =liangshizongxiaofeiliangyuce(X_Niandu,GuanLianDu_Yuzhi,M,N,YuCeNeiRong,Y_liangshichanliang,Y_chengzhenkouliangxiaofeiliang,Y_chengzhensiliaoliangxiaofeiliang,ChengZhenRenKou,ChengZhenHuashuiping,ChengZhenEnGeErXiShu,NongChanPinShengChanJiaGeZhiShu,ChengZhenRenJunShouRu,Y_xiangcunkouliangxiaofeiliang,...
    Y_xiangcunsiliaoliangxiaofeiliang,XiangCunRenKou,XiangCunEnGeErXiShu,XiangCunRenJunShouRu,Y_liangshichanliang_HN,Y_chengzhenkouliangxiaofeiliang_HN,Y_chengzhensiliaoliangxiaofeiliang_HN, ...
    ChengZhenRenKou_HN,ChengZhenHuashuiping_HN,ChengZhenEnGeErXiShu_HN,NongChanPinShengChanJiaGeZhiShu_HN,ChengZhenRenJunShouRu_HN,Y_xiangcunkouliangxiaofeiliang_HN,Y_xiangcunsiliaoliangxiaofeiliang_HN,XiangCunRenKou_HN,XiangCunEnGeErXiShu_HN,XiangCunRenJunShouRu_HN);


Data_Length=length(X_Niandu);

XXXX=X_Niandu;

if M>Data_Length
    M=Data_Length;
end

Z=[Y_chengzhenkouliangxiaofeiliang,Y_chengzhensiliaoliangxiaofeiliang, ...
    ChengZhenRenKou,ChengZhenHuashuiping,ChengZhenEnGeErXiShu,NongChanPinShengChanJiaGeZhiShu,ChengZhenRenJunShouRu,Y_xiangcunkouliangxiaofeiliang, ...
    Y_xiangcunsiliaoliangxiaofeiliang,XiangCunRenKou,XiangCunEnGeErXiShu,XiangCunRenJunShouRu,Y_chengzhenkouliangxiaofeiliang_HN,Y_chengzhensiliaoliangxiaofeiliang_HN, ...
    ChengZhenRenKou_HN,ChengZhenHuashuiping_HN,ChengZhenEnGeErXiShu_HN,NongChanPinShengChanJiaGeZhiShu_HN,ChengZhenRenJunShouRu_HN,...
    Y_xiangcunkouliangxiaofeiliang_HN,Y_xiangcunsiliaoliangxiaofeiliang_HN,XiangCunRenKou_HN,XiangCunEnGeErXiShu_HN,XiangCunRenJunShouRu_HN,Y_liangshichanliang,Y_liangshichanliang_HN];

Y=diff(Z)./Z(1:end-1,:);%做差分，看原始数据是否波动很大
YY=mean(abs(Y));%取波动平均
for j=1:size(Y,2)
    for i=1:size(Y,1)
        if abs(Y(i,j))>YY(j)
            Z(i+1,j)=Z(i,j)*(1+sign(Y(i,j))*YY(j));%如果原始数据前后波动超过平均值，则后一个数据用前一个数据加上波动平均量来代替
        end
    end
end


Y_chengzhenkouliangxiaofeiliang_Tem=Z(:,1);
Y_chengzhensiliaoliangxiaofeiliang_Tem=Z(:,2);
 ChengZhenRenKou_Tem=Z(:,3);
ChengZhenHuashuiping_Tem=Z(:,4);
ChengZhenEnGeErXiShu_Tem=Z(:,5);
NongChanPinShengChanJiaGeZhiShu_Tem=Z(:,6);
ChengZhenRenJunShouRu_Tem=Z(:,7);
Y_xiangcunkouliangxiaofeiliang_Tem=Z(:,8);
Y_xiangcunsiliaoliangxiaofeiliang_Tem=Z(:,9);
XiangCunRenKou_Tem=Z(:,10);
XiangCunEnGeErXiShu_Tem=Z(:,11);
XiangCunRenJunShouRu_Tem=Z(:,12);
Y_chengzhenkouliangxiaofeiliang_HN_Tem=Z(:,13);
Y_chengzhensiliaoliangxiaofeiliang_HN_Tem=Z(:,14);
 ChengZhenRenKou_HN_Tem=Z(:,15);
ChengZhenHuashuiping_HN_Tem=Z(:,16);
ChengZhenEnGeErXiShu_HN_Tem=Z(:,17);
NongChanPinShengChanJiaGeZhiShu_HN_Tem=Z(:,18);
ChengZhenRenJunShouRu_HN_Tem=Z(:,19);
Y_xiangcunkouliangxiaofeiliang_HN_Tem=Z(:,20);
Y_xiangcunsiliaoliangxiaofeiliang_HN_Tem=Z(:,21);
XiangCunRenKou_HN_Tem=Z(:,22);
XiangCunEnGeErXiShu_HN_Tem=Z(:,23);
XiangCunRenJunShouRu_HN_Tem=Z(:,24);

Y_liangshichanliang_Tem=Z(:,25);
Y_liangshichanliang_HN_Tem=Z(:,26);


% YuCeNeiRong:所需预测的内容：10:'全国粮食消费量'，包括：全国乡村口粮消费量','全国城镇口粮消费量','全国乡村饲料粮消费量','全国城镇饲料粮消费量';
%%%                          20:'河南粮食消费量'，包括： '河南乡村口粮消费量','河南城镇口粮消费量','河南乡村饲料粮消费量','河南城镇饲料粮消费量'.

 %全国：10
 %河南：20
 switch YuCeNeiRong
    case 10
    case 20
 end
  switch YuCeNeiRong;
     case 10
    % 全国粮食产量
        % 取原始数据
       Y_liangshichanliang=Y_liangshichanliang(Data_Length-M+1:end);
       % 取处理过的预测前全部数据
       Y_liangshichanliang_Tem=Y_liangshichanliang_Tem(Data_Length-M+1:end);
                % 取处理过的预测前所需数据
       Y_liangshichanliang_YuCe=Y_liangshichanliang_Tem(1:M);
                 
       % 全国城镇口粮消费量
        % 取原始数据
         Y_chengzhenkouliangxiaofeiliang=Y_chengzhenkouliangxiaofeiliang(Data_Length-M+1:end);
         ChengZhenRenKou= ChengZhenRenKou(Data_Length-M+1:end);
         ChengZhenHuashuiping=ChengZhenHuashuiping(Data_Length-M+1:end);
         ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu(Data_Length-M+1:end);
  
        % 取处理过的预测前全部数据
       Y_chengzhenkouliangxiaofeiliang_Tem=Y_chengzhenkouliangxiaofeiliang_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_Tem(Data_Length-M+1:end);
       ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_chengzhenkouliangxiaofeiliang_YuCe=Y_chengzhenkouliangxiaofeiliang_Tem(1:M);
        ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
        ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
         XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
         XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
         XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
    % 全国城镇饲料粮消费量
        % 取原始数据
        Y_chengzhensiliaoliangxiaofeiliang=Y_chengzhensiliaoliangxiaofeiliang(Data_Length-M+1:end);
         ChengZhenRenKou= ChengZhenRenKou(Data_Length-M+1:end);
         ChengZhenHuashuiping=ChengZhenHuashuiping(Data_Length-M+1:end);
         ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu(Data_Length-M+1:end);
  
        % 取处理过的预测前全部数据
       Y_chengzhensiliaoliangxiaofeiliang_Tem=Y_chengzhensiliaoliangxiaofeiliang_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_chengzhensiliaoliangxiaofeiliang_YuCe=Y_chengzhensiliaoliangxiaofeiliang_Tem(1:M);
        ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
        ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
        XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
        XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
   % 全国乡村口粮消费量
        % 取原始数据
         Y_xiangcunkouliangxiaofeiliang=Y_xiangcunkouliangxiaofeiliang(Data_Length-M+1:end);
         ChengZhenRenKou= ChengZhenRenKou(Data_Length-M+1:end);
         ChengZhenHuashuiping=ChengZhenHuashuiping(Data_Length-M+1:end);
         ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu(Data_Length-M+1:end);
  
        % 取处理过的预测前全部数据
        Y_xiangcunkouliangxiaofeiliang_Tem=Y_xiangcunkouliangxiaofeiliang_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_xiangcunkouliangxiaofeiliang_YuCe=Y_xiangcunkouliangxiaofeiliang_Tem(1:M);
        ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
        ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
        XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
        XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
     % 全国乡村饲料粮消费量
        % 取原始数据
        Y_xiangcunsiliaoliangxiaofeiliang=Y_xiangcunsiliaoliangxiaofeiliang(Data_Length-M+1:end);
         ChengZhenRenKou= ChengZhenRenKou(Data_Length-M+1:end);
         ChengZhenHuashuiping=ChengZhenHuashuiping(Data_Length-M+1:end);
         ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu(Data_Length-M+1:end);
         % 取处理过的预测前全部数据
      Y_xiangcunsiliaoliangxiaofeiliang_Tem=Y_xiangcunsiliaoliangxiaofeiliang_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_xiangcunsiliaoliangxiaofeiliang_YuCe=Y_xiangcunsiliaoliangxiaofeiliang_Tem(1:M);
        ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
        ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
        XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
        XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
        
     
       case 20 
           
      % 河南粮食产量
        % 取原始数据
       Y_liangshichanliang=Y_liangshichanliang_HN(Data_Length-M+1:end);
       % 取处理过的预测前全部数据
       Y_liangshichanliang_Tem=Y_liangshichanliang_HN_Tem(Data_Length-M+1:end);
      % 取处理过的预测前所需数据
       Y_liangshichanliang_YuCe=Y_liangshichanliang_HN_Tem(1:M);                                        
        % 河南城镇口粮消费量
        % 取原始数据               
        Y_chengzhenkouliangxiaofeiliang=Y_chengzhenkouliangxiaofeiliang_HN(Data_Length-M+1:end);
        ChengZhenRenKou= ChengZhenRenKou_HN(Data_Length-M+1:end);
       ChengZhenHuashuiping= ChengZhenHuashuiping_HN(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu_HN(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu_HN(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu_HN(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou_HN(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu_HN(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_chengzhenkouliangxiaofeiliang_Tem=Y_chengzhenkouliangxiaofeiliang_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_HN_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_HN_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_HN_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_chengzhenkouliangxiaofeiliang_YuCe=Y_chengzhenkouliangxiaofeiliang_Tem(1:M);
       ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
       ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
         XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
         XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
       % 河南城镇饲料粮消费量
        % 取原始数据
        Y_chengzhensiliaoliangxiaofeiliang=Y_chengzhensiliaoliangxiaofeiliang_HN(Data_Length-M+1:end);
        ChengZhenRenKou= ChengZhenRenKou_HN(Data_Length-M+1:end);
       ChengZhenHuashuiping= ChengZhenHuashuiping_HN(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu_HN(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu_HN(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu_HN(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou_HN(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu_HN(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_chengzhensiliaoliangxiaofeiliang_Tem=Y_chengzhensiliaoliangxiaofeiliang_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_HN_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_HN_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_HN_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
      Y_chengzhensiliaoliangxiaofeiliang_YuCe=Y_chengzhensiliaoliangxiaofeiliang_Tem(1:M);
       ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
       ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
         XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
         XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
      % 河南乡村口粮消费量
        % 取原始数据
        Y_xiangcunkouliangxiaofeiliang= Y_xiangcunkouliangxiaofeiliang_HN(Data_Length-M+1:end);
        ChengZhenRenKou= ChengZhenRenKou_HN(Data_Length-M+1:end);
       ChengZhenHuashuiping= ChengZhenHuashuiping_HN(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu_HN(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu_HN(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu_HN(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou_HN(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu_HN(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_xiangcunkouliangxiaofeiliang_Tem= Y_xiangcunkouliangxiaofeiliang_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_HN_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_HN_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_HN_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_xiangcunkouliangxiaofeiliang_YuCe=Y_xiangcunkouliangxiaofeiliang_Tem(1:M);
       ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
       ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
         XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
         XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M);
       % 河南乡村饲料粮消费量
        % 取原始数据
       Y_xiangcunsiliaoliangxiaofeiliang= Y_xiangcunsiliaoliangxiaofeiliang_HN(Data_Length-M+1:end);
        ChengZhenRenKou= ChengZhenRenKou_HN(Data_Length-M+1:end);
       ChengZhenHuashuiping= ChengZhenHuashuiping_HN(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu=ChengZhenEnGeErXiShu_HN(Data_Length-M+1:end);
         NongChanPinShengChanJiaGeZhiShu=NongChanPinShengChanJiaGeZhiShu_HN(Data_Length-M+1:end);
         ChengZhenRenJunShouRu=ChengZhenRenJunShouRu_HN(Data_Length-M+1:end);
         XiangCunRenKou=XiangCunRenKou_HN(Data_Length-M+1:end);
         XiangCunEnGeErXiShu=XiangCunEnGeErXiShu_HN(Data_Length-M+1:end);
         XiangCunRenJunShouRu=XiangCunRenJunShouRu_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
       Y_xiangcunsiliaoliangxiaofeiliang_Tem= Y_xiangcunsiliaoliangxiaofeiliang_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenKou_Tem= ChengZhenRenKou_HN_Tem(Data_Length-M+1:end);
        ChengZhenHuashuiping_Tem=ChengZhenHuashuiping_HN_Tem(Data_Length-M+1:end);
        ChengZhenEnGeErXiShu_Tem=ChengZhenEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        NongChanPinShengChanJiaGeZhiShu_Tem=NongChanPinShengChanJiaGeZhiShu_HN_Tem(Data_Length-M+1:end);
        ChengZhenRenJunShouRu_Tem=ChengZhenRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenKou_Tem=XiangCunRenKou_HN_Tem(Data_Length-M+1:end);
        XiangCunEnGeErXiShu_Tem=XiangCunEnGeErXiShu_HN_Tem(Data_Length-M+1:end);
        XiangCunRenJunShouRu_Tem= XiangCunRenJunShouRu_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
       Y_xiangcunsiliaoliangxiaofeiliang_YuCe=Y_xiangcunsiliaoliangxiaofeiliang_Tem(1:M);
       ChengZhenRenKou_YuCe=ChengZhenRenKou_Tem(1:M);
        ChengZhenHuashuiping_YuCe=ChengZhenHuashuiping_Tem(1:M);
        ChengZhenEnGeErXiShu_YuCe=ChengZhenEnGeErXiShu_Tem(1:M);
        NongChanPinShengChanJiaGeZhiShu_YuCe=NongChanPinShengChanJiaGeZhiShu_Tem(1:M);
       ChengZhenRenJunShouRu_YuCe=ChengZhenRenJunShouRu_Tem(1:M);
         XiangCunRenKou_YuCe= XiangCunRenKou_Tem(1:M);
        XiangCunEnGeErXiShu_YuCe=XiangCunEnGeErXiShu_Tem(1:M);
         XiangCunRenJunShouRu_YuCe=XiangCunRenJunShouRu_Tem(1:M); 
       end
       M1=M;
for i=1:N

    [Export_Y_liangshichanliang]=HuiSeYuCe(Y_liangshichanliang_YuCe,M1); 
    [Export_Y_chengzhenkouliangxiaofeiliang]=HuiSeYuCe(Y_chengzhenkouliangxiaofeiliang_YuCe,M1); 
    [Export_Y_chengzhensiliaoliangxiaofeiliang]=HuiSeYuCe(Y_chengzhensiliaoliangxiaofeiliang_YuCe,M1); 
    [Export_Y_xiangcunkouliangxiaofeiliang]=HuiSeYuCe(Y_xiangcunkouliangxiaofeiliang_YuCe,M1); 
    [Export_Y_xiangcunsiliaoliangxiaofeiliang]=HuiSeYuCe(Y_xiangcunsiliaoliangxiaofeiliang_YuCe,M1); 
    
    [Export_ChengZhenRenKou]=HuiSeYuCe(ChengZhenRenKou_YuCe,M1); 
    [Export_ChengZhenHuashuiping]=HuiSeYuCe(ChengZhenHuashuiping_YuCe,M1);
    [Export_ChengZhenEnGeErXiShu]=HuiSeYuCe(ChengZhenEnGeErXiShu_YuCe,M1);
    [Export_NongChanPinShengChanJiaGeZhiShu]=HuiSeYuCe(NongChanPinShengChanJiaGeZhiShu_YuCe,M1);
    [Export_ChengZhenRenJunShouRu]=HuiSeYuCe(ChengZhenRenJunShouRu_YuCe,M1);
    [ Export_XiangCunRenKou]=HuiSeYuCe(XiangCunRenKou_YuCe,M1);
    [Export_XiangCunEnGeErXiShu]=HuiSeYuCe(XiangCunEnGeErXiShu_YuCe,M1);
    [Export_XiangCunRenJunShouRu]=HuiSeYuCe(XiangCunRenJunShouRu_YuCe,M1);
    
   Y_liangshichanliang_YuCe=[Y_liangshichanliang_YuCe;Export_Y_liangshichanliang];
   Y_chengzhenkouliangxiaofeiliang_YuCe=[Y_chengzhenkouliangxiaofeiliang_YuCe;Export_Y_chengzhenkouliangxiaofeiliang];
    Y_chengzhensiliaoliangxiaofeiliang_YuCe=[Y_chengzhensiliaoliangxiaofeiliang_YuCe;Export_Y_chengzhensiliaoliangxiaofeiliang];
   Y_xiangcunkouliangxiaofeiliang_YuCe=[Y_xiangcunkouliangxiaofeiliang_YuCe;Export_Y_xiangcunkouliangxiaofeiliang];
   Y_xiangcunsiliaoliangxiaofeiliang_YuCe=[Y_xiangcunsiliaoliangxiaofeiliang_YuCe;Export_Y_xiangcunsiliaoliangxiaofeiliang];
   
    ChengZhenRenKou_YuCe=[ChengZhenRenKou_YuCe;Export_ChengZhenRenKou];
    ChengZhenHuashuiping_YuCe=[ChengZhenHuashuiping_YuCe;Export_ChengZhenHuashuiping];
    ChengZhenEnGeErXiShu_YuCe=[ChengZhenEnGeErXiShu_YuCe;Export_ChengZhenEnGeErXiShu];
    NongChanPinShengChanJiaGeZhiShu_YuCe=[NongChanPinShengChanJiaGeZhiShu_YuCe;Export_NongChanPinShengChanJiaGeZhiShu];
    ChengZhenRenJunShouRu_YuCe=[ChengZhenRenJunShouRu_YuCe; Export_ChengZhenRenJunShouRu];
    XiangCunRenKou_YuCe=[XiangCunRenKou_YuCe; Export_XiangCunRenKou];
    XiangCunEnGeErXiShu_YuCe=[XiangCunEnGeErXiShu_YuCe;Export_XiangCunEnGeErXiShu];
    XiangCunRenJunShouRu_YuCe=[XiangCunRenJunShouRu_YuCe;Export_XiangCunRenJunShouRu];
    M1=M1+1;
end
    
%求取数据的初始像
Y_chengzhenkouliangxiaofeiliang_ChuShiXiang=(Y_chengzhenkouliangxiaofeiliang_YuCe-min(Y_chengzhenkouliangxiaofeiliang_YuCe))/(max(Y_chengzhenkouliangxiaofeiliang_YuCe)-min(Y_chengzhenkouliangxiaofeiliang_YuCe));
Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang=( Y_chengzhensiliaoliangxiaofeiliang_YuCe-min( Y_chengzhensiliaoliangxiaofeiliang_YuCe))/(max( Y_chengzhensiliaoliangxiaofeiliang_YuCe)-min( Y_chengzhensiliaoliangxiaofeiliang_YuCe));
Y_xiangcunkouliangxiaofeiliang_ChuShiXiang=(Y_xiangcunkouliangxiaofeiliang_YuCe-min(Y_xiangcunkouliangxiaofeiliang_YuCe))/(max(Y_xiangcunkouliangxiaofeiliang_YuCe)-min(Y_xiangcunkouliangxiaofeiliang_YuCe));
Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang=(Y_xiangcunsiliaoliangxiaofeiliang_YuCe-min(Y_xiangcunsiliaoliangxiaofeiliang_YuCe))/(max(Y_xiangcunsiliaoliangxiaofeiliang_YuCe)-min(Y_xiangcunsiliaoliangxiaofeiliang_YuCe));

ChengZhenRenKou_ChuShiXiang=(ChengZhenRenKou_YuCe-min(ChengZhenRenKou_YuCe))/(max(ChengZhenRenKou_YuCe)-min(ChengZhenRenKou_YuCe));
ChengZhenHuashuiping_ChuShiXiang=(ChengZhenHuashuiping_YuCe-min(ChengZhenHuashuiping_YuCe))/(max(ChengZhenHuashuiping_YuCe)-min(ChengZhenHuashuiping_YuCe));
ChengZhenEnGeErXiShu_ChuShiXiang=(ChengZhenEnGeErXiShu_YuCe-min(ChengZhenEnGeErXiShu_YuCe))/(max(ChengZhenEnGeErXiShu_YuCe)-min(ChengZhenEnGeErXiShu_YuCe));
NongChanPinShengChanJiaGeZhiShu_ChuShiXiang=(NongChanPinShengChanJiaGeZhiShu_YuCe-min(NongChanPinShengChanJiaGeZhiShu_YuCe))/(max(NongChanPinShengChanJiaGeZhiShu_YuCe)-min(NongChanPinShengChanJiaGeZhiShu_YuCe));
ChengZhenRenJunShouRu_ChuShiXiang=(ChengZhenRenJunShouRu_YuCe-min(ChengZhenRenJunShouRu_YuCe))/(max(ChengZhenRenJunShouRu_YuCe)-min(ChengZhenRenJunShouRu_YuCe));
XiangCunRenKou_ChuShiXiang=(XiangCunRenKou_YuCe-min(XiangCunRenKou_YuCe))/(max(XiangCunRenKou_YuCe)-min(XiangCunRenKou_YuCe));
XiangCunEnGeErXiShu_ChuShiXiang=(XiangCunEnGeErXiShu_YuCe-min(XiangCunEnGeErXiShu_YuCe))/(max(XiangCunEnGeErXiShu_YuCe)-min(XiangCunEnGeErXiShu_YuCe));
XiangCunRenJunShouRu_ChuShiXiang=(XiangCunRenJunShouRu_YuCe-min(XiangCunRenJunShouRu_YuCe))/(max(XiangCunRenJunShouRu_YuCe)-min(XiangCunRenJunShouRu_YuCe));
%数据的初始像计算完毕
%数据用一阶缓冲算子处理
for i=1:M+N
   Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1(i,1)=sum(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang(i:end))/(M+N-i+1);
     Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1(i,1)=sum(Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang(i:end))/(M+N-i+1);
      Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1(i,1)=sum(Y_xiangcunkouliangxiaofeiliang_ChuShiXiang(i:end))/(M+N-i+1);
       Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1(i,1)=sum(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang(i:end))/(M+N-i+1);
   
       ChengZhenRenKou_ChuShiXiang1(i,1)=sum( ChengZhenRenKou_ChuShiXiang(i:end))/(M+N-i+1);
    ChengZhenHuashuiping_ChuShiXiang1(i,1)=sum(ChengZhenHuashuiping_ChuShiXiang(i:end))/(M+N-i+1);
    ChengZhenEnGeErXiShu_ChuShiXiang1(i,1)=sum(ChengZhenEnGeErXiShu_ChuShiXiang(i:end))/(M+N-i+1);
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1(i,1)=sum(NongChanPinShengChanJiaGeZhiShu_ChuShiXiang(i:end))/(M+N-i+1);
    ChengZhenRenJunShouRu_ChuShiXiang1(i,1)=sum(ChengZhenRenJunShouRu_ChuShiXiang(i:end))/(M+N-i+1);
     XiangCunRenKou_ChuShiXiang1(i,1)=sum( XiangCunRenKou_ChuShiXiang(i:end))/(M+N-i+1);
      XiangCunEnGeErXiShu_ChuShiXiang1(i,1)=sum(XiangCunEnGeErXiShu_ChuShiXiang(i:end))/(M+N-i+1);
       XiangCunRenJunShouRu_ChuShiXiang1(i,1)=sum(XiangCunRenJunShouRu_ChuShiXiang(i:end))/(M+N-i+1);
end
%数据处理完毕  

%计算关联系数及关联度
Delta_1=[abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-ChengZhenRenKou_ChuShiXiang1),abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-ChengZhenHuashuiping_ChuShiXiang1),abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-ChengZhenEnGeErXiShu_ChuShiXiang1), ...
    abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1),abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-ChengZhenRenJunShouRu_ChuShiXiang1),abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1- XiangCunRenKou_ChuShiXiang1), ...
    abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-  XiangCunEnGeErXiShu_ChuShiXiang1), abs(Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1-   XiangCunRenJunShouRu_ChuShiXiang1)];
GuanLianXiShu_1=(min(min(Delta_1))+0.5*max(max(Delta_1)))./(Delta_1+0.5*max(max(Delta_1)));%关联系数
GuanLianDu_1=mean(GuanLianXiShu_1);%城镇口粮关联度
Delta_2=[abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenRenKou_ChuShiXiang1),abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenHuashuiping_ChuShiXiang1),abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenEnGeErXiShu_ChuShiXiang1), ...
    abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1),abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenRenJunShouRu_ChuShiXiang1),abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1- XiangCunRenKou_ChuShiXiang1), ...
    abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-  XiangCunEnGeErXiShu_ChuShiXiang1), abs( Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1-   XiangCunRenJunShouRu_ChuShiXiang1)];
GuanLianXiShu_2=(min(min(Delta_2))+0.5*max(max(Delta_2)))./(Delta_2+0.5*max(max(Delta_2)));%关联系数
GuanLianDu_2=mean(GuanLianXiShu_2);%城镇饲料粮关联度
Delta_3=[abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-ChengZhenRenKou_ChuShiXiang1),abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-ChengZhenHuashuiping_ChuShiXiang1),abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-ChengZhenEnGeErXiShu_ChuShiXiang1), ...
    abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1),abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-ChengZhenRenJunShouRu_ChuShiXiang1),abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1- XiangCunRenKou_ChuShiXiang1), ...
    abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-  XiangCunEnGeErXiShu_ChuShiXiang1), abs( Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1-   XiangCunRenJunShouRu_ChuShiXiang1)];
GuanLianXiShu_3=(min(min(Delta_3))+0.5*max(max(Delta_3)))./(Delta_3+0.5*max(max(Delta_3)));%关联系数
GuanLianDu_3=mean(GuanLianXiShu_3);%乡村口粮关联度
Delta_4=[abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenRenKou_ChuShiXiang1),abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenHuashuiping_ChuShiXiang1),abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenEnGeErXiShu_ChuShiXiang1), ...
    abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1),abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-ChengZhenRenJunShouRu_ChuShiXiang1),abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1- XiangCunRenKou_ChuShiXiang1), ...
    abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-  XiangCunEnGeErXiShu_ChuShiXiang1), abs(Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1-   XiangCunRenJunShouRu_ChuShiXiang1)];
GuanLianXiShu_4=(min(min(Delta_4))+0.5*max(max(Delta_4)))./(Delta_4+0.5*max(max(Delta_4)));%关联系数
GuanLianDu_4=mean(GuanLianXiShu_4);%关联度


 [X_Niandu_YuCe,Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe,Y_chengzhenkouliangxiaofeiliang_DuoYuanYuCe,Y_chengzhenkouliangxiaofeiliang_DuoYuanYuCe_Xiuzheng,GuanLianDu_1, ...
    GuanLianXiShu_1,GuanLianXiShu_1_Name,GuanLianXiShu_1_YuCe,GuanLianXiShu_1_YuCe_Name] ...
    =LiangShiXiaoFeiLiangYuCe(X_Niandu,Y_chengzhenkouliangxiaofeiliang,Y_chengzhenkouliangxiaofeiliang_YuCe,Y_chengzhenkouliangxiaofeiliang_ChuShiXiang1, GuanLianDu_1,GuanLianXiShu_1,ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1, ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1, XiangCunRenKou_ChuShiXiang1,XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1,M,N,GuanLianDu_Yuzhi);
[X_Niandu_YuCe,Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe,Y_chengzhensiliaoliangxiaofeiliang_DuoYuanYuCe,Y_chengzhensiliaoliangxiaofeiliang_DuoYuanYuCe_Xiuzheng,GuanLianDu_2, ...
    GuanLianXiShu_2,GuanLianXiShu_2_Name,GuanLianXiShu_2_YuCe,GuanLianXiShu_2_YuCe_Name] ...
    =LiangShiXiaoFeiLiangYuCe(X_Niandu,Y_chengzhensiliaoliangxiaofeiliang,Y_chengzhensiliaoliangxiaofeiliang_YuCe,Y_chengzhensiliaoliangxiaofeiliang_ChuShiXiang1, GuanLianDu_2,GuanLianXiShu_2,ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1, ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1,XiangCunRenKou_ChuShiXiang1,XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1,M,N,GuanLianDu_Yuzhi);
[X_Niandu_YuCe,Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe,Y_xiangcunkouliangxiaofeiliang_DuoYuanYuCe,Y_xiangcunkouliangxiaofeiliang_DuoYuanYuCe_Xiuzheng,GuanLianDu_3, ...
    GuanLianXiShu_3,GuanLianXiShu_3_Name,GuanLianXiShu_3_YuCe,GuanLianXiShu_3_YuCe_Name] ...
    =LiangShiXiaoFeiLiangYuCe(X_Niandu,Y_xiangcunkouliangxiaofeiliang,Y_xiangcunkouliangxiaofeiliang_YuCe,Y_xiangcunkouliangxiaofeiliang_ChuShiXiang1,GuanLianDu_3,GuanLianXiShu_3,ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1, ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1,XiangCunRenKou_ChuShiXiang1, XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1,M,N,GuanLianDu_Yuzhi);
[X_Niandu_YuCe,Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe,Y_xiangcunsiliaoliangxiaofeiliang_DuoYuanYuCe, Y_xiangcunsiliaoliangxiaofeiliang_DuoYuanYuCe_Xiuzheng,GuanLianDu_4, ...
    GuanLianXiShu_4,GuanLianXiShu_4_Name,GuanLianXiShu_4_YuCe,GuanLianXiShu_4_YuCe_Name] ...
    =LiangShiXiaoFeiLiangYuCe(X_Niandu,Y_xiangcunsiliaoliangxiaofeiliang,Y_xiangcunsiliaoliangxiaofeiliang_YuCe,Y_xiangcunsiliaoliangxiaofeiliang_ChuShiXiang1,GuanLianDu_4,GuanLianXiShu_4,ChengZhenRenKou_ChuShiXiang1,ChengZhenHuashuiping_ChuShiXiang1,ChengZhenEnGeErXiShu_ChuShiXiang1, ...
    NongChanPinShengChanJiaGeZhiShu_ChuShiXiang1,ChengZhenRenJunShouRu_ChuShiXiang1,XiangCunRenKou_ChuShiXiang1,XiangCunEnGeErXiShu_ChuShiXiang1,XiangCunRenJunShouRu_ChuShiXiang1,M,N,GuanLianDu_Yuzhi);


% %%%实际消费量
% 
% Y_gongyeyongliang=Y_liangshichanliang*0.06;
% Y_sunhao=Y_liangshichanliang*0.04;
% Y_zhongzi=Y_liangshichanliang*0.05;
% Y_siliaoliangzongxiaofeiliang=Y_chengzhenkouliangxiaofeiliang+Y_xiangcunkouliangxiaofeiliang;
% Y_kouliangzongxiaofeiliang=Y_chengzhensiliaoliangxiaofeiliang+Y_xiangcunsiliaoliangxiaofeiliang;
% Y_liangshizongxiaofeiliang=Y_chengzhenkouliangxiaofeiliang+Y_chengzhensiliaoliangxiaofeiliang+Y_xiangcunkouliangxiaofeiliang+Y_xiangcunsiliaoliangxiaofeiliang+Y_gongyeyongliang+Y_sunhao+Y_zhongzi;


%%%预测总消费量

Y_gongyeyongliang_HuiSeYuCe=Y_liangshichanliang_YuCe(M+1:end)*0.06;
Y_sunhao_HuiSeYuCe=Y_liangshichanliang_YuCe(M+1:end)*0.04;
Y_zhongzi_HuiSeYuCe=Y_liangshichanliang_YuCe(M+1:end)*0.05;
Y_kouliangzongxiaofeiliang_HuiSeYuCe=Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe+Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe;
Y_siliaoliangzongxiaofeiliang_HuiSeYuCe=Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe+Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe;
Y_liangshizongxiaofeiliang_HuiSeYuCe=Y_chengzhenkouliangxiaofeiliang_HuiSeYuCe+Y_chengzhensiliaoliangxiaofeiliang_HuiSeYuCe+Y_xiangcunkouliangxiaofeiliang_HuiSeYuCe+Y_xiangcunsiliaoliangxiaofeiliang_HuiSeYuCe+Y_gongyeyongliang_HuiSeYuCe+Y_sunhao_HuiSeYuCe+Y_zhongzi_HuiSeYuCe;





