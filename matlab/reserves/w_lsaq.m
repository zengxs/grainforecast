function y = w_lsaq(X_Niandu, Y_Shijichanliang, m, n)

    % 年份预处理
    x = length(X_Niandu);
    X_Niandu1=zeros(1,x+n);
    for i=1:x+n
        if i<=x
            X_Niandu1(i)=X_Niandu(i);
        else
            X_Niandu1(i)=X_Niandu1(i-1)+1;
        end
    end

    % 倒退年份预处理
    if m > x
        m = x;
    end

    y = lsaq(X_Niandu1, Y_Shijichanliang, n, m);
    y=y(end-n+1:end);
end
