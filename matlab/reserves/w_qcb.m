function [y1 y2 y3] = w_qcb(X_Niandu, Y_Shijichanliang, m, n, aqxs)

    % 年份预处理
    x = length(X_Niandu);
    X_Niandu1=zeros(1,x+n);
    for i=1:x+n
        if i<=x
            X_Niandu1(i)=X_Niandu(i);
        else
            X_Niandu1(i)=X_Niandu1(i-1)+1;
        end
    end

    % 倒退年份预处理
    if m > x
        m = x;
    end

    [y1 y2 y3] = qeceblmx(X_Niandu1, Y_Shijichanliang, n, m, aqxs);
    y1=y1(end-n+1:end);
    y2=y2(end-n+1:end);
    y3=y3(end-n+1:end);

end
