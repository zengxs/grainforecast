/*
 * MATLAB Compiler: 4.11 (R2009b)
 * Date: Wed Nov  1 00:26:17 2017
 * Arguments: "-B" "macro_default" "-B" "csharedlib:libreserves" "-W"
 * "lib:libreserves" "-T" "link:lib" "w_bdzs.m" "w_jjsszs.m" "w_lsaq.m"
 * "w_qcb.m" 
 */

#ifndef __libreserves_h
#define __libreserves_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_libreserves
#define PUBLIC_libreserves_C_API __global
#else
#define PUBLIC_libreserves_C_API /* No import statement needed. */
#endif

#define LIB_libreserves_C_API PUBLIC_libreserves_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_libreserves
#define PUBLIC_libreserves_C_API __declspec(dllexport)
#else
#define PUBLIC_libreserves_C_API __declspec(dllimport)
#endif

#define LIB_libreserves_C_API PUBLIC_libreserves_C_API


#else

#define LIB_libreserves_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_libreserves_C_API 
#define LIB_libreserves_C_API /* No special import/export declaration */
#endif

extern LIB_libreserves_C_API 
bool MW_CALL_CONV libreservesInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_libreserves_C_API 
bool MW_CALL_CONV libreservesInitialize(void);

extern LIB_libreserves_C_API 
void MW_CALL_CONV libreservesTerminate(void);



extern LIB_libreserves_C_API 
void MW_CALL_CONV libreservesPrintStackTrace(void);

extern LIB_libreserves_C_API 
bool MW_CALL_CONV mlxW_bdzs(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libreserves_C_API 
bool MW_CALL_CONV mlxW_jjsszs(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libreserves_C_API 
bool MW_CALL_CONV mlxW_lsaq(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libreserves_C_API 
bool MW_CALL_CONV mlxW_qcb(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libreserves_C_API 
long MW_CALL_CONV libreservesGetMcrID();



extern LIB_libreserves_C_API bool MW_CALL_CONV mlfW_bdzs(int nargout, mxArray** y1, mxArray** y2, mxArray* X_Niandu, mxArray* Y_Shijichanliang, mxArray* NCRK, mxArray* NCRJKL, mxArray* CZRK, mxArray* CZRJKL, mxArray* NCDLXF, mxArray* NCQLXF, mxArray* NCRLXF, mxArray* NCNLXF, mxArray* CZDLXF, mxArray* CZQLXF, mxArray* CZRLXF, mxArray* CZNLXF, mxArray* CK, mxArray* JK, mxArray* m, mxArray* n, mxArray* bdsx, mxArray* bdxx);

extern LIB_libreserves_C_API bool MW_CALL_CONV mlfW_jjsszs(int nargout, mxArray** y, mxArray* X_Niandu, mxArray* m, mxArray* n, mxArray* Y_Shijichanliang, mxArray* NCRK, mxArray* NCRJKL, mxArray* CZRK, mxArray* CZRJKL, mxArray* NCDLXF, mxArray* NCQLXF, mxArray* NCRLXF, mxArray* NCNLXF, mxArray* CZDLXF, mxArray* CZQLXF, mxArray* CZRLXF, mxArray* CZNLXF, mxArray* CK, mxArray* JK);

extern LIB_libreserves_C_API bool MW_CALL_CONV mlfW_lsaq(int nargout, mxArray** y, mxArray* X_Niandu, mxArray* Y_Shijichanliang, mxArray* m, mxArray* n);

extern LIB_libreserves_C_API bool MW_CALL_CONV mlfW_qcb(int nargout, mxArray** y1, mxArray** y2, mxArray** y3, mxArray* X_Niandu, mxArray* Y_Shijichanliang, mxArray* m, mxArray* n, mxArray* aqxs);

#ifdef __cplusplus
}
#endif
#endif
