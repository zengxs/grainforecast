function [y1 y2] = w_bdzs(X_Niandu, Y_Shijichanliang, ...
    NCRK, NCRJKL, CZRK, CZRJKL, NCDLXF, NCQLXF, NCRLXF, NCNLXF, CZDLXF, CZQLXF, CZRLXF, CZNLXF, CK, JK, ...
    m, n, bdsx, bdxx)

    % 年份预处理
    x = length(X_Niandu);
    X_Niandu1=zeros(1,x+n);
    for i=1:x+n
        if i<=x
            X_Niandu1(i)=X_Niandu(i);
        else
            X_Niandu1(i)=X_Niandu1(i-1)+1;
        end
    end

    % 倒退年份预处理
    if m > x
        m = x;
    end

    y1 = bdzs(X_Niandu1, Y_Shijichanliang, m, n, bdsx, bdxx);
    y2 = bdzsxz2(X_Niandu, X_Niandu1, Y_Shijichanliang, m, n, ...
        NCRK, NCRJKL, CZRK, CZRJKL, NCDLXF, NCQLXF, NCRLXF, NCNLXF, ...
        CZDLXF, CZQLXF, CZRLXF, CZNLXF, CK, JK, bdsx, bdxx);

    y1=y1(end-n+1:end);
    y2=y2(end-n+1:end);

end
