function y=xfgy(G0,ycn3)
%Y=[46217 45264 45706 43070 46947 48402 49746 50160 52871 53082 54648 57121 58958];%2000-2012粮食产量数据
Y1=hdycwcxz(G0,ycn3);
bl=zeros;
bl(1)=0.08;
bl(2)=0.08;
bl(3)=0.08;
bl(4)=0.08;
for i=5:length(Y1)
    bl(i)=(1+0.05).*bl(i-1);
end
bl;
GYYL=zeros;
m=length(Y1);
for i=1:m
    GYYL(i)=Y1(i).*bl(i);
end
GYYL;
y=GYYL;
