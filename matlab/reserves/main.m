clear all;
clc;
load OriginalData1.mat X_Niandu Y_Shijichanliang Y_Shijichanliang1 Y_Liangshixiaofei NCRK NCRJKL CZRK CZRJKL NCDLXF NCQLXF NCRLXF NCNLXF CZDLXF CZQLXF CZRLXF CZNLXF LSCL ZZYL CK JK...
    n0 daotuinianfen1 bdsx bdxx bdxzsx bdxzxx qeceblaqxs qeceblxzaqxs;
%*******************以下是入口参数，可以修改

%*************************入口参数结束

%if n0<1
%    n=5;
%else
%    n=10;
%end
%n

switch n0
    case 0
        n=5;
    case 1
        n=10;
end


%*************************生成横坐标，即要显示的年份，包括预测年份
m=length(X_Niandu);
X_Niandu1=zeros(1,m+n);
for i=1:m+n
    if i<=m
        X_Niandu1(i)=X_Niandu(i);
    else
        X_Niandu1(i)=X_Niandu1(i-1)+1;
    end
end
% X_Niandu1;
%***********************************横坐标生成结束

%***********************************判断输入倒退年分是否符合要求，因为数据只有1980-2013年，若超过此范围使
%用全部数据，若在此范围之内，则使用实际数据
% X_Niandu(end)
if daotuinianfen1<=X_Niandu(end)-1980
    daotuinianfen=daotuinianfen1;
else 
    daotuinianfen=X_Niandu(end)-1980+1;
end
%*************************************判断结束
% daotuinianfen

%**************************************以下为6种储备模型    
mx1=bdzs(X_Niandu1,Y_Shijichanliang,daotuinianfen,n,bdsx,bdxx);%波动指数法粮食储备量预测

[mx2]=bdzsxz2(X_Niandu,X_Niandu1,Y_Shijichanliang,daotuinianfen,n,NCRK,NCRJKL,CZRK,CZRJKL,NCDLXF,NCQLXF,NCRLXF,NCNLXF,CZDLXF,CZQLXF,CZRLXF,CZNLXF,CK,JK,bdsx,bdxx);%基于粮食消费的波动指数法粮食储备量预测

[mx31,mx32,mx33]=qeceblmx(X_Niandu1,Y_Shijichanliang,n,daotuinianfen,qeceblaqxs); % 基于全额、差额、比例模型的粮食储备量预测

[mx5]=jjsszx1(X_Niandu,X_Niandu1,Y_Shijichanliang,NCRK,NCRJKL,CZRK,CZRJKL,NCDLXF,NCQLXF,NCRLXF,NCNLXF,CZDLXF,CZQLXF,CZRLXF,CZNLXF,CK,JK,n,daotuinianfen);%基于经济损失最小的粮食储备量预测（对应专利3）

mx6=lsaq(X_Niandu1,Y_Shijichanliang,n,daotuinianfen);%基于粮食安全的粮食储备量预测（对应专利4）
%*******************************************储备模型结束

figure;
subplot(2,2,1);
plot(X_Niandu1,mx1,'-r*');
%plot(X_Niandu1(end-xfcd):X_Niandu1(end),mx1(end-xfcd:end),'-r*');
hold on
plot(X_Niandu1,mx2,'-ob');
legend('基于产量的波动指数','基于消费的波动指数',4);
xlabel('年份');
ylabel('粮食储备量/万吨');
title({'波动指数粮食储备粮模型和基于粮食消费的波动指数粮食储备粮模型';['波动上限 = ',num2str(bdsx),', 波动下限 = ',num2str(bdxx)]});

subplot(2,2,2);
plot(X_Niandu1,mx31,'-r*'); 
%text(2005,Sq(26)+500,'全额模型')
hold on
plot(X_Niandu1,mx32,'-*b'); 
%text(1985,Sc(6)-500,'差额模型')
hold on
plot(X_Niandu1,mx33,'-ok'); 
%text(2010,Sb(31)+500,'比例模型')
xlabel('年份');
ylabel('粮食储备量/万吨');
B=num2str(qeceblaqxs);
title({'基于消费的全额、差额、比例模型';['安全系数= ',num2str(qeceblaqxs)]}); % 此处想把安全系数alpha中的数据显示出来
legend('全额模型','差额模型','比例模型',4);

subplot(2,2,3);
plot(X_Niandu1, mx5);
xlabel('年份');
ylabel('粮食储备量/万吨');
title('经济损失最小粮食储备模型');

subplot(2,2,4);
plot(X_Niandu1, mx6);
xlabel('年份');
ylabel('粮食储备数量/万吨');
title('基于粮食安全的粮食储备模型');
