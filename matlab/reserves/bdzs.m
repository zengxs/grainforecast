function y1=bdzs(X,Y0,d,ycn,sx,xx)

%m=length(Y0);
Y=Y0(end-d+1:end); % 取2000-2012年数据
YH=hdycwcxz(Y,ycn);%对上述数据使用灰度误差修正法做预测
n=length(YH);
YS=[Y0 YH(n-ycn+1:end)];

%plot(X,Y,'-r*');%画出X,Y的趋势图
p=polyfit(X,YS,1); %线性拟合
Y1=polyval(p,X);%计算趋势产量
%plot(X,YS,'-r*',X,Y1,'-b'); 
V=(YS-Y1)./Y1*100;%计算波动系数
%Q=zeros(32,1);
alpha=sx;%定义波动上限**************************修改波动上限
beta=xx;%定义波动下限**************************修改波动下限
for n=1:length(X)
    if V(n)<beta
        Q(n)=(YS(n)-Y1(n)).*(1-beta./V(n));
    elseif beta<=V(n)& V(n)<alpha
        Q(n)=0;
    else
        Q(n)=(YS(n)-Y1(n)).*(1-alpha./V(n));
    end
end
S=zeros;
%计算累计吞吐量
for j=1:length(X)
    S(j)=sum(Q(1:j));
    tunru=max(S);%计算吞吐量的最大值
    tuchu=min(S);%计算吞吐量的最小值
    c(j)=max(abs(tunru),abs(tuchu));
end
y1=c(end-d-ycn+1:end);%粮食存储量
%figure(1)
%plot(X(end-d-ycn+1:end),y1);
%xlabel('年份');
%ylabel('粮食储备量/万吨');
%title({'波动指数粮食储备粮模型';['波动上限=',num2str(alpha)];['波动下限=',num2str(beta)]});
%以下为二次拟合
%f=poly2str(polyfit(X,Y,2),'X');
