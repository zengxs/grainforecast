function y=hdycwcxz(X0,yc)

for i=1:length(X0)-1
   X01(i)=X0(i)./X0(i+1);
end
X01;
%产生累加生成列X1
X1=cumsum(X0);
%构造数据矩阵B和数据向量Y
for k=2:length(X0)
    z(k)=(1/2).*(X1(k)+X1(k-1));
end
z;
B=[(-z(2:end))' ones(length(z)-1,1)];
Y=(X0(2:end))';
%计算模型参数beta=（a b）'
bata=inv(B'*B)*B'*Y;
a=bata(1);
b=bata(2);
a;
b;
%写出GM(1，1)预测模型
X2(1)=X1(1);
for m=1:length(X0)
    X2(m+1)=(X0(1)-b/a)*exp(-a*m)+b/a;
end
X2;
%还原累减生成列X3
X3(1)=X2(1);
for m=2:length(X0)
    X3(m)=(1-exp(a)).*(X0(1)-b./a).*exp(-a*m);
end
X3;

Delta01=abs(X0-X3) ;              %计算相对残差绝对值
Phi=Delta01./X0;                  %计算相对相对残差
Delta0=X0-X3;                %计算相对残差
CAZX=min(Delta0);          %找残差最小值
Delta2=Delta0+abs(CAZX);    %将残差变成正值
%计算残差的后5项
%for i=length(Delta1)-4:length(Delta1) 
%Delta2(i+1-(length(Delta1)-4))=Delta1(i)
%end
%Delta2
%产生累加生成列X1
Delta3=cumsum(Delta2);
%构造数据矩阵B和数据向量Y
for k=2:length(Delta2)
    z1(k)=(1/2).*(Delta3(k)+Delta3(k-1));
end
z1;
B1=[(-z1(2:end))' ones(length(z1)-1,1)];
Y1=(Delta2(2:end))';
%计算模型参数beta=（a b）'
bata1=inv(B1'*B1)*B1'*Y1;
a1=bata1(1);
b1=bata1(2);
a1;
b1;
c1=b1/a1;
d1=Delta0(length(Delta0)-4)-b1/a1;
%写出GM(1，1)预测模型
Delta4(1)=Delta3(1);
for m=1:length(Delta2)
   Delta4(m+1)=(Delta0(1)-b1/a1)*exp(-a1*m)+b1/a1;
end
Delta4;
%还原累减生成列Delta5
%Delta5(1)=Delta4(1);
%for m=2:length(Delta2)
%    Delta5(m)=Delta4(m)-Delta4(m-1);
%end
%Delta5
%还原累减生成列Delta5
Delta5(1)=Delta4(1);
for m=2:length(Delta2)
    Delta5(m)=(1-exp(a1))*(Delta0(1)-b1/a1)*exp(-a1*m);
end
Delta5;


%恢复残差序列，即加上最小值
for i=1:length(Delta5)
    Q(i)=Delta5(i)+CAZX;
end
Q;
%用残差修正预测序列的后5项，即残差补偿
for i=1:length(Delta5)
    X4(i)=X3(i)+Q(i);
end
X4;
%X5=X3
%用X4代换X3的后5项
%for i=1:length(X4)
%    X5(length(X5)-5+i)=X4(i);
%end
%X5
CC0=X0-X3 ;               %计算残差
CC1=X0-X4 ;               %计算残差
XDCC1=CC0./X0;            %计算相对残差
XDCC1=CC1./X0;           %计算相对残差
X5=X4;
%预测(即序列)
for m=length(X0)+1:length(X0)+yc
    X3(m)=(1-exp(a)).*(X0(1)-b./a).*exp(-a*m);
    Delta5(m)=(1-exp(a1))*(Delta0(1)-b1/a1)*exp(-a1*m);
    Q(m)=Delta5(m)+CAZX;
    X5(m)=X3(m)+Q(m);
end
X3;
Delta5;
Q;
X5;
y=X5;
