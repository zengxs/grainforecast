function y = w_jjsszs(X_Niandu, m, n, ...
    Y_Shijichanliang,NCRK,NCRJKL,CZRK,CZRJKL,NCDLXF,NCQLXF,NCRLXF,NCNLXF,CZDLXF,CZQLXF,CZRLXF,CZNLXF,CK,JK)

    % 年份预处理
    x = length(X_Niandu);
    X_Niandu1=zeros(1,x+n);
    for i=1:x+n
        if i<=x
            X_Niandu1(i)=X_Niandu(i);
        else
            X_Niandu1(i)=X_Niandu1(i-1)+1;
        end
    end

    % 倒退年份预处理
    if m > x
        m = x;
    end

    y = jjsszx1(X_Niandu,X_Niandu1,Y_Shijichanliang,NCRK,NCRJKL,CZRK,CZRJKL, ...
        NCDLXF,NCQLXF,NCRLXF,NCNLXF,CZDLXF,CZQLXF,CZRLXF,CZNLXF,CK,JK,n,m);
    y=y(end-n+1:end);
end
