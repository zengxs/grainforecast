function y6=lsaq(X,Y0,ycn,d)

%m=length(Y0);
Y=Y0(end-d+1:end);%取2000-2012年数据
YH=hdycwcxz(Y,ycn);%对上述数据使用灰度误差修正法做预测
%n=length(YH);
YS=[Y0 YH(end-ycn+1:end)];%1980-2017年粮食产量

A=0.454;%单位粮食短缺经济损失
C=0.312;%单位粮食储量成本
rz=0.83;%粮食自给率
rj=1-rz;%粮食进口率
pz=0.1;%粮食自给不足概率
pj=0.2;%粮食进口不足概率
%lscl=hdycwcxz(LSCL);%2003-2017粮食产量的预测
s=abs(A.*(pz+rj.*(pj-pz))/(A.*((pz+pj)-pz.*pj)-C));
cl=YS.*s;
y6=cl(end-ycn-d+1:end);
