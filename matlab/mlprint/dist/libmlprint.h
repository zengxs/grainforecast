/*
 * MATLAB Compiler: 4.11 (R2009b)
 * Date: Wed Nov  1 00:00:27 2017
 * Arguments: "-B" "macro_default" "-B" "csharedlib:libmlprint" "-W"
 * "lib:libmlprint" "-T" "link:lib" "mlprint.m" "-d" "dist" 
 */

#ifndef __libmlprint_h
#define __libmlprint_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_libmlprint
#define PUBLIC_libmlprint_C_API __global
#else
#define PUBLIC_libmlprint_C_API /* No import statement needed. */
#endif

#define LIB_libmlprint_C_API PUBLIC_libmlprint_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_libmlprint
#define PUBLIC_libmlprint_C_API __declspec(dllexport)
#else
#define PUBLIC_libmlprint_C_API __declspec(dllimport)
#endif

#define LIB_libmlprint_C_API PUBLIC_libmlprint_C_API


#else

#define LIB_libmlprint_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_libmlprint_C_API 
#define LIB_libmlprint_C_API /* No special import/export declaration */
#endif

extern LIB_libmlprint_C_API 
bool MW_CALL_CONV libmlprintInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_libmlprint_C_API 
bool MW_CALL_CONV libmlprintInitialize(void);

extern LIB_libmlprint_C_API 
void MW_CALL_CONV libmlprintTerminate(void);



extern LIB_libmlprint_C_API 
void MW_CALL_CONV libmlprintPrintStackTrace(void);

extern LIB_libmlprint_C_API 
bool MW_CALL_CONV mlxMlprint(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[]);

extern LIB_libmlprint_C_API 
long MW_CALL_CONV libmlprintGetMcrID();



extern LIB_libmlprint_C_API bool MW_CALL_CONV mlfMlprint(int nargout, mxArray** char_array, mxArray* input);

#ifdef __cplusplus
}
#endif
#endif
