%从后往前取数据，采用采用多元一次回归模型，区间归一化，增加新信息型，残差修正
clear all;
clc;
load OriginalData.mat X_Niandu Y_Shijichanliang LiangShiZuoWuBoZhongZongMianJi ZongLaoDongLi HuaFeiZongYongLiang YouXiaoGuanGaiMianJi YongDianZongLiang JiXieZongDongLi ShouZaiZongMianJi ...
    DaoGu_ShiJiChanliang XiaoMai_ShiJiChanliang YuMi_ShiJiChanliang JuMin_LiangShiXiaoFeiliang ChengzhenRenkou NongcunRenkou ChengzhenEnGeErXiShu NongCunEnGeErXiShu ...
    DaoGuBoZhongZongMianJi XiaoMaiBoZhongZongMianJi YuMiBoZhongZongMianJi ...
    Y_Shijichanliang_HN LiangShiZuoWuBoZhongZongMianJi_HN ZongLaoDongLi_HN HuaFeiZongYongLiang_HN YouXiaoGuanGaiMianJi_HN YongDianZongLiang_HN JiXieZongDongLi_HN ShouZaiZongMianJi_HN ...
    DaoGu_ShiJiChanliang_HN XiaoMai_ShiJiChanliang_HN YuMi_ShiJiChanliang_HN NongZuoWuBoZhongZongMianJi_HN DaoGuBoZhongZongMianJi_HN XiaoMaiBoZhongZongMianJi_HN YuMiBoZhongZongMianJi_HN;
%--------初始化所用数据------------%

% 全国粮食：10
% 全国稻谷：11
% 全国小麦：12
% 全国玉米：13
% 河南粮食：20
% 河南稻谷：21
% 河南小麦：22
% 河南玉米：23
YuCeNeiRong=23; % 河南玉米
M=length(X_Niandu);
N=10;%预测年数，可以随意设置
switch YuCeNeiRong;
    case 13     % 全国玉米
        if N==5
            M=10;
        elseif N==10
            M=5;
        end
      case 12   % 全国小麦
       if N==5
            M=5;
        elseif N==10
            M=5;
       end
       case 21 % 河南稻谷
       if N==5
            M=5;
        elseif N==10
            M=5;
       end
        
end
[X_Niandu_YuCe,Y_Shijichanliang,Y_Shijichanliang_HuiSeYuCe,Y_Shijichanliang_DuoYuanYuCe,Y_Shijichanliang_DuoYuanYuCe_Xiuzheng,Data_Length,GuanLianDu, ...
    GuanLianXiShu,GuanLianXiShu_Name,GuanLianXiShu_YuCe,GuanLianXiShu_YuCe_Name,M] ...
    =LiangShiChanLiangYuCe(X_Niandu,Y_Shijichanliang, ...
    LiangShiZuoWuBoZhongZongMianJi,ZongLaoDongLi,HuaFeiZongYongLiang,YouXiaoGuanGaiMianJi,YongDianZongLiang,JiXieZongDongLi,ShouZaiZongMianJi,DaoGu_ShiJiChanliang,XiaoMai_ShiJiChanliang,YuMi_ShiJiChanliang,JuMin_LiangShiXiaoFeiliang, ...
    ChengzhenRenkou,NongcunRenkou,ChengzhenEnGeErXiShu,NongCunEnGeErXiShu,DaoGuBoZhongZongMianJi,XiaoMaiBoZhongZongMianJi,YuMiBoZhongZongMianJi,Y_Shijichanliang_HN,LiangShiZuoWuBoZhongZongMianJi_HN,ZongLaoDongLi_HN, ...
    HuaFeiZongYongLiang_HN,YouXiaoGuanGaiMianJi_HN,YongDianZongLiang_HN,JiXieZongDongLi_HN,ShouZaiZongMianJi_HN,DaoGu_ShiJiChanliang_HN,XiaoMai_ShiJiChanliang_HN,YuMi_ShiJiChanliang_HN,NongZuoWuBoZhongZongMianJi_HN, ...
    DaoGuBoZhongZongMianJi_HN,XiaoMaiBoZhongZongMianJi_HN,YuMiBoZhongZongMianJi_HN,YuCeNeiRong,M,N);


figure;

subplot(2,1,1)
bar(X_Niandu_YuCe,[Y_Shijichanliang_HuiSeYuCe,Y_Shijichanliang_DuoYuanYuCe,Y_Shijichanliang_DuoYuanYuCe_Xiuzheng]);
grid;
legend('灰色预测年度产量','DI-AFS多元预测年度产量','DI-AFS-EM多元预测年度产量',-1);% DI-AFS表示“差分及影响因子自动筛选”，DI-AFS-EM表示“差分及影响因子自动筛选及残差修正”
title([YuCeNeiRong,'产量预测值']);
xlabel('年度');ylabel('粮食产量（万吨）');

subplot(2,1,2)
bar(X_Niandu_YuCe,GuanLianXiShu(M+1:end,:));ylim([0 1.2])
grid;
legend(GuanLianXiShu_Name,-1);
title([YuCeNeiRong,'产量预测影响因子关联系数']);
xlabel('年度');ylabel('关联系数');

%%
