%从后往前取数据，采用采用多元一次回归模型，区间归一化，增加新信息型，残差修正

function [X_Niandu_YuCe,Y_Shijichanliang,Y_Shijichanliang_HuiSeYuCe,Y_Shijichanliang_DuoYuanYuCe,Y_Shijichanliang_DuoYuanYuCe_Xiuzheng,Data_Length,GuanLianDu, ...
    GuanLianXiShu,GuanLianXiShu_Name,GuanLianXiShu_YuCe,GuanLianXiShu_YuCe_Name,M] ...
    =LiangShiChanLiangYuCe(X_Niandu,Y_Shijichanliang, ...
    LiangShiZuoWuBoZhongZongMianJi,ZongLaoDongLi,HuaFeiZongYongLiang,YouXiaoGuanGaiMianJi,YongDianZongLiang,JiXieZongDongLi,ShouZaiZongMianJi,DaoGu_ShiJiChanliang,XiaoMai_ShiJiChanliang,YuMi_ShiJiChanliang,JuMin_LiangShiXiaoFeiliang, ...
    ChengzhenRenkou,NongcunRenkou,ChengzhenEnGeErXiShu,NongCunEnGeErXiShu,DaoGuBoZhongZongMianJi,XiaoMaiBoZhongZongMianJi,YuMiBoZhongZongMianJi,Y_Shijichanliang_HN,LiangShiZuoWuBoZhongZongMianJi_HN,ZongLaoDongLi_HN, ...
    HuaFeiZongYongLiang_HN,YouXiaoGuanGaiMianJi_HN,YongDianZongLiang_HN,JiXieZongDongLi_HN,ShouZaiZongMianJi_HN,DaoGu_ShiJiChanliang_HN,XiaoMai_ShiJiChanliang_HN,YuMi_ShiJiChanliang_HN,NongZuoWuBoZhongZongMianJi_HN, ...
    DaoGuBoZhongZongMianJi_HN,XiaoMaiBoZhongZongMianJi_HN,YuMiBoZhongZongMianJi_HN,YuCeNeiRong,M,N);

% YuCeNeiRong 此处填写所需预测的内容：'全国粮食','全国小麦','全国稻谷','全国玉米','河南粮食','河南小麦','河南稻谷','河南玉米'.
% M 取数据的年数， N 预测年数，可以随意设置

Data_Length=length(X_Niandu);
XXXX=X_Niandu;
GuanLianDu_Yuzhi=0.8;
if M>Data_Length
    M=Data_Length;
end

Z=[Y_Shijichanliang,LiangShiZuoWuBoZhongZongMianJi,ZongLaoDongLi,HuaFeiZongYongLiang,YouXiaoGuanGaiMianJi,YongDianZongLiang,JiXieZongDongLi,ShouZaiZongMianJi,DaoGu_ShiJiChanliang,XiaoMai_ShiJiChanliang,YuMi_ShiJiChanliang, ...
    JuMin_LiangShiXiaoFeiliang,ChengzhenRenkou,NongcunRenkou,ChengzhenEnGeErXiShu,NongCunEnGeErXiShu,DaoGuBoZhongZongMianJi,XiaoMaiBoZhongZongMianJi,YuMiBoZhongZongMianJi,Y_Shijichanliang_HN,LiangShiZuoWuBoZhongZongMianJi_HN, ...
    ZongLaoDongLi_HN,HuaFeiZongYongLiang_HN,YouXiaoGuanGaiMianJi_HN,YongDianZongLiang_HN,JiXieZongDongLi_HN,ShouZaiZongMianJi_HN,DaoGu_ShiJiChanliang_HN,XiaoMai_ShiJiChanliang_HN,YuMi_ShiJiChanliang_HN,NongZuoWuBoZhongZongMianJi_HN, ...
    DaoGuBoZhongZongMianJi_HN,XiaoMaiBoZhongZongMianJi_HN,YuMiBoZhongZongMianJi_HN];

Y=diff(Z)./Z(1:end-1,:);%做差分，看原始数据是否波动很大
YY=mean(abs(Y));%取波动平均
for j=1:size(Y,2)
    for i=1:size(Y,1)
        if abs(Y(i,j))>YY(j)
            Z(i+1,j)=Z(i,j)*(1+sign(Y(i,j))*YY(j));%如果原始数据前后波动超过平均值，则后一个数据用前一个数据加上波动平均量来代替
        end
    end
end

Y_Shijichanliang_Tem=Z(:,1);
LiangShiZuoWuBoZhongZongMianJi_Tem=Z(:,2);
ZongLaoDongLi_Tem=Z(:,3);
HuaFeiZongYongLiang_Tem=Z(:,4);
YouXiaoGuanGaiMianJi_Tem=Z(:,5);
YongDianZongLiang_Tem=Z(:,6);
JiXieZongDongLi_Tem=Z(:,7);
ShouZaiZongMianJi_Tem=Z(:,8);
DaoGu_ShiJiChanliang_Tem=Z(:,9);
XiaoMai_ShiJiChanliang_Tem=Z(:,10);
YuMi_ShiJiChanliang_Tem=Z(:,11);
JuMin_LiangShiXiaoFeilian_Tem=Z(:,12);
ChengzhenRenkou_Tem=Z(:,13);
NongcunRenkou_Tem=Z(:,14);
ChengzhenEnGeErXiShu_Tem=Z(:,15);
NongCunEnGeErXiShu_Tem=Z(:,16);
DaoGuBoZhongZongMianJi_Tem=Z(:,17);
XiaoMaiBoZhongZongMianJi_Tem=Z(:,18);
YuMiBoZhongZongMianJi_Tem=Z(:,19);
Y_Shijichanliang_HN_Tem=Z(:,20);
LiangShiZuoWuBoZhongZongMianJi_HN_Tem=Z(:,21);
ZongLaoDongLi_HN_Tem=Z(:,22);
HuaFeiZongYongLiang_HN_Tem=Z(:,23);
YouXiaoGuanGaiMianJi_HN_Tem=Z(:,24);
YongDianZongLiang_HN_Tem=Z(:,25);
JiXieZongDongLi_HN_Tem=Z(:,26);
ShouZaiZongMianJi_HN_Tem=Z(:,27);
DaoGu_ShiJiChanliang_HN_Tem=Z(:,28);
XiaoMai_ShiJiChanliang_HN_Tem=Z(:,29);
YuMi_ShiJiChanliang_HN_Tem=Z(:,30);
NongZuoWuBoZhongZongMianJi_HN_Tem=Z(:,31);
DaoGuBoZhongZongMianJi_HN_Tem=Z(:,32);
XiaoMaiBoZhongZongMianJi_HN_Tem=Z(:,33);
YuMiBoZhongZongMianJi_HN_Tem=Z(:,34);


switch YuCeNeiRong;
    case 10 % 全国粮食
        % 取原始数据
        Y_Shijichanliang=Y_Shijichanliang(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=LiangShiZuoWuBoZhongZongMianJi(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=Y_Shijichanliang_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=LiangShiZuoWuBoZhongZongMianJi_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
    case 12 % 全国小麦
        % 取原始数据
        Y_Shijichanliang=XiaoMai_ShiJiChanliang(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=XiaoMaiBoZhongZongMianJi(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi(Data_Length-M+1:end);
         % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=XiaoMai_ShiJiChanliang_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=XiaoMaiBoZhongZongMianJi_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
    case 11 % 全国稻谷
        % 取原始数据
        Y_Shijichanliang=DaoGu_ShiJiChanliang(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=DaoGuBoZhongZongMianJi(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi(Data_Length-M+1:end);
         % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=DaoGu_ShiJiChanliang_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=DaoGuBoZhongZongMianJi_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
    case 13 % 全国玉米
        % 取原始数据
        Y_Shijichanliang=YuMi_ShiJiChanliang(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=YuMiBoZhongZongMianJi(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi(Data_Length-M+1:end);
         % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=YuMi_ShiJiChanliang_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=YuMiBoZhongZongMianJi_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
    case 20 % 河南粮食
        % 取原始数据
        Y_Shijichanliang=Y_Shijichanliang_HN(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=LiangShiZuoWuBoZhongZongMianJi_HN(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi_HN(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang_HN(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi_HN(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang_HN(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi_HN(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=Y_Shijichanliang_HN_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=LiangShiZuoWuBoZhongZongMianJi_HN_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_HN_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_HN_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_HN_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_HN_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_HN_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
        case 22 % 河南小麦
        % 取原始数据
        Y_Shijichanliang=XiaoMai_ShiJiChanliang_HN(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=XiaoMaiBoZhongZongMianJi_HN(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi_HN(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang_HN(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi_HN(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang_HN(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi_HN(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=XiaoMai_ShiJiChanliang_HN_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=XiaoMaiBoZhongZongMianJi_HN_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_HN_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_HN_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_HN_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_HN_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_HN_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
        case 21 % 河南稻谷
        % 取原始数据
        Y_Shijichanliang=DaoGu_ShiJiChanliang_HN(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=DaoGuBoZhongZongMianJi_HN(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi_HN(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang_HN(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi_HN(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang_HN(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi_HN(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=DaoGu_ShiJiChanliang_HN_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=DaoGuBoZhongZongMianJi_HN_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_HN_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_HN_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_HN_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_HN_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_HN_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
        case 23 % 河南玉米
        % 取原始数据
        Y_Shijichanliang=YuMi_ShiJiChanliang_HN(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi=YuMiBoZhongZongMianJi_HN(Data_Length-M+1:end);
        ZongLaoDongLi=ZongLaoDongLi_HN(Data_Length-M+1:end);
        HuaFeiZongYongLiang=HuaFeiZongYongLiang_HN(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi=YouXiaoGuanGaiMianJi_HN(Data_Length-M+1:end);
        YongDianZongLiang=YongDianZongLiang_HN(Data_Length-M+1:end);
        JiXieZongDongLi=JiXieZongDongLi_HN(Data_Length-M+1:end);
        ShouZaiZongMianJi=ShouZaiZongMianJi_HN(Data_Length-M+1:end);
        % 取处理过的预测前全部数据
        Y_Shijichanliang_Tem=YuMi_ShiJiChanliang_HN_Tem(Data_Length-M+1:end);
        LiangShiZuoWuBoZhongZongMianJi_Tem=YuMiBoZhongZongMianJi_HN_Tem(Data_Length-M+1:end);
        ZongLaoDongLi_Tem=ZongLaoDongLi_HN_Tem(Data_Length-M+1:end);
        HuaFeiZongYongLiang_Tem=HuaFeiZongYongLiang_HN_Tem(Data_Length-M+1:end);
        YouXiaoGuanGaiMianJi_Tem=YouXiaoGuanGaiMianJi_HN_Tem(Data_Length-M+1:end);
        YongDianZongLiang_Tem=YongDianZongLiang_HN_Tem(Data_Length-M+1:end);
        JiXieZongDongLi_Tem=JiXieZongDongLi_HN_Tem(Data_Length-M+1:end);
        ShouZaiZongMianJi_Tem=ShouZaiZongMianJi_HN_Tem(Data_Length-M+1:end);
        % 取处理过的预测前所需数据
        Y_Shijichanliang_YuCe=Y_Shijichanliang_Tem(1:M);
        LiangShiZuoWuBoZhongZongMianJi_YuCe=LiangShiZuoWuBoZhongZongMianJi_Tem(1:M);
        ZongLaoDongLi_YuCe=ZongLaoDongLi_Tem(1:M);
        HuaFeiZongYongLiang_YuCe=HuaFeiZongYongLiang_Tem(1:M);
        YouXiaoGuanGaiMianJi_YuCe=YouXiaoGuanGaiMianJi_Tem(1:M);
        YongDianZongLiang_YuCe=YongDianZongLiang_Tem(1:M);
        JiXieZongDongLi_YuCe=JiXieZongDongLi_Tem(1:M);
        ShouZaiZongMianJi_YuCe=ShouZaiZongMianJi_Tem(1:M);
end

M1=M;
for i=1:N
    [Export_Y_Shijichanliang]=HuiSeYuCe(Y_Shijichanliang_YuCe,M1); 
    [Export_LiangShiZuoWuBoZhongZongMianJi]=HuiSeYuCe(LiangShiZuoWuBoZhongZongMianJi_YuCe,M1); 
    [Export_ZongLaoDongLi]=HuiSeYuCe(ZongLaoDongLi_YuCe,M1);
    [Export_HuaFeiZongYongLiang]=HuiSeYuCe(HuaFeiZongYongLiang_YuCe,M1);
    [Export_YouXiaoGuanGaiMianJi]=HuiSeYuCe(YouXiaoGuanGaiMianJi_YuCe,M1);
    [Export_YongDianZongLiang]=HuiSeYuCe(YongDianZongLiang_YuCe,M1);
    [Export_JiXieZongDongLi]=HuiSeYuCe(JiXieZongDongLi_YuCe,M1);
    [Export_ShouZaiZongMianJi]=HuiSeYuCe(ShouZaiZongMianJi_YuCe,M1);
    
    Y_Shijichanliang_YuCe=[Y_Shijichanliang_YuCe;Export_Y_Shijichanliang];
    LiangShiZuoWuBoZhongZongMianJi_YuCe=[LiangShiZuoWuBoZhongZongMianJi_YuCe;Export_LiangShiZuoWuBoZhongZongMianJi];
    ZongLaoDongLi_YuCe=[ZongLaoDongLi_YuCe;Export_ZongLaoDongLi];
    HuaFeiZongYongLiang_YuCe=[HuaFeiZongYongLiang_YuCe;Export_HuaFeiZongYongLiang];
    YouXiaoGuanGaiMianJi_YuCe=[YouXiaoGuanGaiMianJi_YuCe;Export_YouXiaoGuanGaiMianJi];
    YongDianZongLiang_YuCe=[YongDianZongLiang_YuCe;Export_YongDianZongLiang];
    JiXieZongDongLi_YuCe=[JiXieZongDongLi_YuCe;Export_JiXieZongDongLi];
    ShouZaiZongMianJi_YuCe=[ShouZaiZongMianJi_YuCe;Export_ShouZaiZongMianJi];
    M1=M1+1;
end
    
%求取数据的初始像
Y_Shijichanliang_ChuShiXiang=(Y_Shijichanliang_YuCe-min(Y_Shijichanliang_YuCe))/(max(Y_Shijichanliang_YuCe)-min(Y_Shijichanliang_YuCe));
LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang=(LiangShiZuoWuBoZhongZongMianJi_YuCe-min(LiangShiZuoWuBoZhongZongMianJi_YuCe))/(max(LiangShiZuoWuBoZhongZongMianJi_YuCe)-min(LiangShiZuoWuBoZhongZongMianJi_YuCe));
ZongLaoDongLi_ChuShiXiang=(ZongLaoDongLi_YuCe-min(ZongLaoDongLi_YuCe))/(max(ZongLaoDongLi_YuCe)-min(ZongLaoDongLi_YuCe));
HuaFeiZongYongLiang_ChuShiXiang=(HuaFeiZongYongLiang_YuCe-min(HuaFeiZongYongLiang_YuCe))/(max(HuaFeiZongYongLiang_YuCe)-min(HuaFeiZongYongLiang_YuCe));
YouXiaoGuanGaiMianJi_ChuShiXiang=(YouXiaoGuanGaiMianJi_YuCe-min(YouXiaoGuanGaiMianJi_YuCe))/(max(YouXiaoGuanGaiMianJi_YuCe)-min(YouXiaoGuanGaiMianJi_YuCe));
YongDianZongLiang_ChuShiXiang=(YongDianZongLiang_YuCe-min(YongDianZongLiang_YuCe))/(max(YongDianZongLiang_YuCe)-min(YongDianZongLiang_YuCe));
JiXieZongDongLi_ChuShiXiang=(JiXieZongDongLi_YuCe-min(JiXieZongDongLi_YuCe))/(max(JiXieZongDongLi_YuCe)-min(JiXieZongDongLi_YuCe));
ShouZaiZongMianJi_ChuShiXiang=(ShouZaiZongMianJi_YuCe-min(ShouZaiZongMianJi_YuCe))/(max(ShouZaiZongMianJi_YuCe)-min(ShouZaiZongMianJi_YuCe));
%数据的初始像计算完毕
%数据用一阶缓冲算子处理
for i=1:M+N
    Y_Shijichanliang_ChuShiXiang1(i,1)=sum(Y_Shijichanliang_ChuShiXiang(i:end))/(M+N-i+1);
    LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang1(i,1)=sum(LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang(i:end))/(M+N-i+1);
    ZongLaoDongLi_ChuShiXiang1(i,1)=sum(ZongLaoDongLi_ChuShiXiang(i:end))/(M+N-i+1);
    HuaFeiZongYongLiang_ChuShiXiang1(i,1)=sum(HuaFeiZongYongLiang_ChuShiXiang(i:end))/(M+N-i+1);
    YouXiaoGuanGaiMianJi_ChuShiXiang1(i,1)=sum(YouXiaoGuanGaiMianJi_ChuShiXiang(i:end))/(M+N-i+1);
    YongDianZongLiang_ChuShiXiang1(i,1)=sum(YongDianZongLiang_ChuShiXiang(i:end))/(M+N-i+1);
    JiXieZongDongLi_ChuShiXiang1(i,1)=sum(JiXieZongDongLi_ChuShiXiang(i:end))/(M+N-i+1);
    ShouZaiZongMianJi_ChuShiXiang1(i,1)=sum(ShouZaiZongMianJi_ChuShiXiang(i:end))/(M+N-i+1);
end
%数据处理完毕
%计算关联系数及关联度
Delta=[abs(Y_Shijichanliang_ChuShiXiang1-LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang1),abs(Y_Shijichanliang_ChuShiXiang1-ZongLaoDongLi_ChuShiXiang1),abs(Y_Shijichanliang_ChuShiXiang1-HuaFeiZongYongLiang_ChuShiXiang1), ...
    abs(Y_Shijichanliang_ChuShiXiang1-YouXiaoGuanGaiMianJi_ChuShiXiang1),abs(Y_Shijichanliang_ChuShiXiang1-YongDianZongLiang_ChuShiXiang1),abs(Y_Shijichanliang_ChuShiXiang1-JiXieZongDongLi_ChuShiXiang1), ...
    abs(Y_Shijichanliang_ChuShiXiang1-ShouZaiZongMianJi_ChuShiXiang1)];
GuanLianXiShu=(min(min(Delta))+0.5*max(max(Delta)))./(Delta+0.5*max(max(Delta)));%关联系数
GuanLianDu=mean(GuanLianXiShu);%关联度
%多元回归

X1=[ones(M,1),LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang1(1:M),ZongLaoDongLi_ChuShiXiang1(1:M),HuaFeiZongYongLiang_ChuShiXiang1(1:M), ...
    YouXiaoGuanGaiMianJi_ChuShiXiang1(1:M),YongDianZongLiang_ChuShiXiang1(1:M),JiXieZongDongLi_ChuShiXiang1(1:M),ShouZaiZongMianJi_ChuShiXiang1(1:M)];
% 筛选影响因子，只保留关联度大的影子
XXX1=X1(:,1);
for i=1:size(GuanLianDu,2)
    if GuanLianDu(i)>GuanLianDu_Yuzhi
        XXX1=[XXX1,X1(:,i+1)];
    end
end
%筛选影响因子完毕
B1=regress(Y_Shijichanliang_ChuShiXiang1(1:M),XXX1);% 多元一次线性回归

%-------开始预测---------

X2=[ones(M+N,1),LiangShiZuoWuBoZhongZongMianJi_ChuShiXiang1,ZongLaoDongLi_ChuShiXiang1,HuaFeiZongYongLiang_ChuShiXiang1,YouXiaoGuanGaiMianJi_ChuShiXiang1,YongDianZongLiang_ChuShiXiang1, ...
    JiXieZongDongLi_ChuShiXiang1,ShouZaiZongMianJi_ChuShiXiang1];
% 筛选影响因子，只保留关联度大的影子
XXX2=X2(:,1);
X3={'粮食作物播种面积','总劳动力','化肥总用量','有效灌溉面积','用电总量','机械总动力','受灾总面积'};
X4=[];% 用于预测的影响因子关联系数
X5={};% 用于预测的影响因子名称
for i=1:size(GuanLianDu,2)
    if GuanLianDu(i)>GuanLianDu_Yuzhi
        XXX2=[XXX2,X2(:,i+1)];
        X4=[X4,GuanLianXiShu(:,i)];
        X5=[X5,X3{i}];
    end
end
GuanLianXiShu_Name=X3;
GuanLianXiShu_YuCe=X4;
GuanLianXiShu_YuCe_Name=X5;
%筛选影响因子完毕
Shijichanliang_HuiSeYuCe_Temp2=XXX2*B1;%多元一次线性回归预测产量


%一阶缓冲算子逆处理
Shijichanliang_HuiSeYuCe_Temp3(M+N,1)=Shijichanliang_HuiSeYuCe_Temp2(end,1);
for i=M+N-1:-1:1
    Shijichanliang_HuiSeYuCe_Temp3(i,1)=Shijichanliang_HuiSeYuCe_Temp2(i,1)*(M+N-i+1)-sum(Shijichanliang_HuiSeYuCe_Temp3(i+1:end));
end
%一阶缓冲算子处理完毕
%初始像逆运算
Shijichanliang_HuiSeYuCe=Shijichanliang_HuiSeYuCe_Temp3*(max(Y_Shijichanliang_YuCe)-min(Y_Shijichanliang_YuCe))+min(Y_Shijichanliang_YuCe);
%初始像逆运算完毕

%残差修正
Error_Juedui=abs(Y_Shijichanliang(1:M)-Shijichanliang_HuiSeYuCe(1:M));
M1=M;
for i=1:N
    [Error1]=HuiSeYuCe(Error_Juedui,M1); 
    Error_Juedui=[Error_Juedui;Error1];
    M1=M1+1;
end
Shijichanliang_HuiSeYuCe_Xiuzheng=Shijichanliang_HuiSeYuCe+sign(Y_Shijichanliang_YuCe-Shijichanliang_HuiSeYuCe).*Error_Juedui;

%残差修正结束
%----保存预测数据-----
for i=1:N
   X_Niandu_YuCe(i)=X_Niandu(end)+i;
end

Y_Shijichanliang_HuiSeYuCe=Y_Shijichanliang_YuCe(M+1:end);%----产量预测-----
Y_Shijichanliang_DuoYuanYuCe=Shijichanliang_HuiSeYuCe(M+1:end);
Y_Shijichanliang_DuoYuanYuCe_Xiuzheng=Shijichanliang_HuiSeYuCe_Xiuzheng(M+1:end);

