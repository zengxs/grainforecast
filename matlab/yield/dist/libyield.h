/*
 * MATLAB Compiler: 4.11 (R2009b)
 * Date: Wed Nov  1 00:17:59 2017
 * Arguments: "-B" "macro_default" "-B" "csharedlib:libyield" "-W"
 * "lib:libyield" "-T" "link:lib" "LiangShiChanLiangYuCe.m" 
 */

#ifndef __libyield_h
#define __libyield_h 1

#if defined(__cplusplus) && !defined(mclmcrrt_h) && defined(__linux__)
#  pragma implementation "mclmcrrt.h"
#endif
#include "mclmcrrt.h"
#ifdef __cplusplus
extern "C" {
#endif

#if defined(__SUNPRO_CC)
/* Solaris shared libraries use __global, rather than mapfiles
 * to define the API exported from a shared library. __global is
 * only necessary when building the library -- files including
 * this header file to use the library do not need the __global
 * declaration; hence the EXPORTING_<library> logic.
 */

#ifdef EXPORTING_libyield
#define PUBLIC_libyield_C_API __global
#else
#define PUBLIC_libyield_C_API /* No import statement needed. */
#endif

#define LIB_libyield_C_API PUBLIC_libyield_C_API

#elif defined(_HPUX_SOURCE)

#ifdef EXPORTING_libyield
#define PUBLIC_libyield_C_API __declspec(dllexport)
#else
#define PUBLIC_libyield_C_API __declspec(dllimport)
#endif

#define LIB_libyield_C_API PUBLIC_libyield_C_API


#else

#define LIB_libyield_C_API

#endif

/* This symbol is defined in shared libraries. Define it here
 * (to nothing) in case this isn't a shared library. 
 */
#ifndef LIB_libyield_C_API 
#define LIB_libyield_C_API /* No special import/export declaration */
#endif

extern LIB_libyield_C_API 
bool MW_CALL_CONV libyieldInitializeWithHandlers(
       mclOutputHandlerFcn error_handler, 
       mclOutputHandlerFcn print_handler);

extern LIB_libyield_C_API 
bool MW_CALL_CONV libyieldInitialize(void);

extern LIB_libyield_C_API 
void MW_CALL_CONV libyieldTerminate(void);



extern LIB_libyield_C_API 
void MW_CALL_CONV libyieldPrintStackTrace(void);

extern LIB_libyield_C_API 
bool MW_CALL_CONV mlxLiangShiChanLiangYuCe(int nlhs, mxArray *plhs[], int nrhs, mxArray 
                                           *prhs[]);

extern LIB_libyield_C_API 
long MW_CALL_CONV libyieldGetMcrID();



extern LIB_libyield_C_API bool MW_CALL_CONV mlfLiangShiChanLiangYuCe(int nargout, mxArray** X_Niandu_YuCe, mxArray** Y_Shijichanliang, mxArray** Y_Shijichanliang_HuiSeYuCe, mxArray** Y_Shijichanliang_DuoYuanYuCe, mxArray** Y_Shijichanliang_DuoYuanYuCe_Xiuzheng, mxArray** Data_Length, mxArray** GuanLianDu, mxArray** GuanLianXiShu, mxArray** GuanLianXiShu_Name, mxArray** GuanLianXiShu_YuCe, mxArray** GuanLianXiShu_YuCe_Name, mxArray** M, mxArray* X_Niandu, mxArray* Y_Shijichanliang_in1, mxArray* LiangShiZuoWuBoZhongZongMianJi, mxArray* ZongLaoDongLi, mxArray* HuaFeiZongYongLiang, mxArray* YouXiaoGuanGaiMianJi, mxArray* YongDianZongLiang, mxArray* JiXieZongDongLi, mxArray* ShouZaiZongMianJi, mxArray* DaoGu_ShiJiChanliang, mxArray* XiaoMai_ShiJiChanliang, mxArray* YuMi_ShiJiChanliang, mxArray* JuMin_LiangShiXiaoFeiliang, mxArray* ChengzhenRenkou, mxArray* NongcunRenkou, mxArray* ChengzhenEnGeErXiShu, mxArray* NongCunEnGeErXiShu, mxArray* DaoGuBoZhongZongMianJi, mxArray* XiaoMaiBoZhongZongMianJi, mxArray* YuMiBoZhongZongMianJi, mxArray* Y_Shijichanliang_HN, mxArray* LiangShiZuoWuBoZhongZongMianJi_HN, mxArray* ZongLaoDongLi_HN, mxArray* HuaFeiZongYongLiang_HN, mxArray* YouXiaoGuanGaiMianJi_HN, mxArray* YongDianZongLiang_HN, mxArray* JiXieZongDongLi_HN, mxArray* ShouZaiZongMianJi_HN, mxArray* DaoGu_ShiJiChanliang_HN, mxArray* XiaoMai_ShiJiChanliang_HN, mxArray* YuMi_ShiJiChanliang_HN, mxArray* NongZuoWuBoZhongZongMianJi_HN, mxArray* DaoGuBoZhongZongMianJi_HN, mxArray* XiaoMaiBoZhongZongMianJi_HN, mxArray* YuMiBoZhongZongMianJi_HN, mxArray* YuCeNeiRong, mxArray* M_in1, mxArray* N);

#ifdef __cplusplus
}
#endif
#endif
