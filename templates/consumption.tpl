<!DOCTYPE html>
<html lang="zh-hans">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width" />
    <title>消费量预测 - 数字化模拟粮食储备辅助决策系统</title>
    <link rel="stylesheet" href="/static/style.css" />
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  </head>
  <body>
    <!--网站头部-->
    <header>
      <section id="header-banners">
        <section id="header-banner-title">
          <div class="container">
            <img alt="logo" src="/static/images/logo.png" />
          </div>
        </section>
      </section>
      <nav>
        <div class="container">
          <ul>
            <li>
              <a href="/">
              <img alt="home-icon" src="/static/images/icon-home.png" /> 首页
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/yield">
              <img alt="icon-price" src="/static/images/icon-dashboard.png" /> 产量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li class="active">
              <a href="/consumption">
              <img alt="icon-price" src="/static/images/icon-price.png" /> 消费量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/reserves">
              <img alt="icon-price" src="/static/images/icon-search.png" /> 储备量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/admin">
              <img alt="icon-price" src="/static/images/icon-person.png" /> 后台管理
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!--主体部分-->
    <article>
      <div class="container-fluid">
        <!-- 左侧说明框 -->
        <div class="col-md-2">
          <div class="panel option-panel">
            <div class="banner">
              <div class="banner-title">消费量预测</div>
            </div>
            <div class="body" style="height: 960px; overflow-y: scroll;">
              <h5>目标与要求：</h5>
              <p>随着我国人口的持续增长，城市化、工业化进程的不断推进，居民收入的提高以及生活方式的转变，我国粮食消费需求将呈刚性增长趋势，
                而土地、劳动力、资本、技术等生产要素从农村不断向城市转移，未来相当的一段时期，我国粮食安全既面临着粮食生产所需的自然资源与环境条件不佳、
                新能源发展等严峻形势的挑战，同时也面临着人口刚性增长以及经济快速发展所带来的粮食需求增长的压力。深入分析我国粮食消费情况及变化趋势，
                对今后我国发展粮食生产，提高资源利用效率，保障国家粮食安全具有重要意义。
              </p>
            </div>
          </div>
        </div>
        <div class="container col-md-10">
          <div id="yield col-md-12">
            <div class="panel chart-panel col-md-9">
              <div class="banner">
                <div class="banner-title">
                  <span>消费量预测</span>
                  <i class="banner-title-end"></i>
                </div>
              </div>
              <div class="body">
                <div class="chart" id="chart" style="height: 640px;"></div>
              </div>
            </div>
            <div class="panel option-panel col-md-3">
              <div class="banner">
                <div class="banner-title">选项</div>
              </div>
              <div class="body">
                <div class="options">
                  <ul>
                    <li>
                      <label>预测类型</label>
                      <select id="predict-type">
                        <option value="全国" selected>全国消费量</option>
                        <option value="河南">河南消费量</option>
                      </select>
                    </li>
                    <li>
                      <label>预测期限</label>
                      <select id="predict-term">
                        <option value="short" selected>中短期</option>
                        <option value="long">中长期</option>
                      </select>
                    </li>
                    <li>
                      <label>关联度下限</label>
                      <input id="gldxx" type="text" value="0.7" />
                    </li>
                    <br/>
                    <li>
                      <label>显示类型</label>
                      <select id="chart-type">
                        <option value="consumption" selected>总消费量</option>
                        <option disabled>---------------</option>
                        <option value="consumption_czt">口粮消费量</option>
                        <option value="consumption_xct">饲料粮消费量</option>
                        <option value="consumption_zz">种子用粮</option>
                        <option value="consumption_gyyl">工业用粮</option>
                        <option value="consumption_sh">损耗用粮</option>
                        <option disabled>---------------</option>
                        <option value="consumption_glxs_ck">城镇口粮 - 关联系数</option>
                        <option value="consumption_glxs_cs">城镇饲料粮 - 关联系数</option>
                        <option value="consumption_glxs_xk">乡村口粮 - 关联系数</option>
                        <option value="consumption_glxs_xs">乡村饲料粮 - 关联系数</option>
                      </select>
                    </li>
                    <br/>
                    <li>
                      <input id="predict" type="submit" value="预测" style="background: url(/static/images/button.png) 0 0 no-repeat; width: 75px; height: 30px; line-height: 30px; text-align: center; margin: 10px auto 0; color: #fff; cursor: pointer; border: 0 none;"  />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="panel option-panel col-md-12">
            <div class="banner">
              <div class="banner-title">模型详情</div>
            </div>
            <div class="body" style="height: 270px; overflow-y: scroll;">
              <h4><strong>联合动态预测模型：</strong></h4>
              <h5><strong>模型描述</strong></h5>
              <p>该模型是从营养目标出发估算粮食消费量。具体实现方法是分别计算城乡口粮消费量、城乡饲料粮消费量、工业用粮消费量、种子用粮量、
                损耗用粮，然后加总求出年度粮食总消费量。首先对城乡口粮消费量及其影响因素(人口数量、城镇化水平、恩格尔系数以及农产品生产价格指数)，
                城乡饲料粮消费量及其影响因素(人口数量、城镇化水平以及居民收入)的关联度进行定量计算，根据设定的阈值选择主要影响因素。
                然后采用灰色系统模型预测影响因素的变化趋势，并联合多元回归模型实现粮食消费量的中长期动态预测。
              </p>
              <h5><strong>预测步骤</strong></h5>
              <ol>
                <li>
                  <p>计算口粮消费量和饲料粮消费量。用城乡人均粮食消费量乘以城乡人口数量，二者加总得到城乡口粮消费量。
                    饲料粮需求量由人均肉类、蛋类、奶类和水产类乘以城乡人口数，再按照肉料比1:3.7，蛋料比1:2.7，奶料比1:0.5，水产料比1:0.4换算得到。
                  </p>
                </li>
                <li>
                  <p>对历年口粮和饲料粮消费量及其影响因素的数据进行波动性检验，根据波动量对上述数据进行平滑处理。</p>
                </li>
                <li>
                  <p>分别运用ARIMA模型和灰色系统模型对影响因子数据进行预测。</p>
                </li>
                <li>
                  <p>选取关联度大的影响因子并联合多元回归模型分别构建城乡口粮消费量和饲料粮消费量的预测模型。</p>
                </li>
                <li>
                  <p>分别按照粮食产量的相应比例估算工业用粮，种子用粮和损耗用粮。</p>
                </li>
                <li>
                  <p>将预测得到的城乡口粮消费量、城乡饲料粮消费量、工业用粮、种子用粮和损耗用粮相加得到粮食总消费量。</p>
                </li>
              </ol>
              <h4><strong>基于口粮比例的粮食消费量预测方法：</strong></h4>
              <h5><strong>模型描述</strong></h5>
              <p>首先根据城镇人口、农村人口、城镇人口人均粮食消费和农村人口人均粮食消费计算口粮消费总量，并根据一定的比例关系转化为原粮消费量。
                然后计算口粮原粮消费在历年粮食消费总量中的比例，从而建立比例系数的变化规律模型。最后，根据对口粮原粮的数据预测和系数变化模型推算粮食消费变化规律。
              </p>
              <h5><strong>特点说明</strong></h5>
              <p>由于人口和人均粮食消费数据易于精准统计，由此可以得到较为准确的口粮消费数据。同时口粮消费在粮食消费总量中所占比例变化较为稳定，具有很强的规律性。
                因此该粮食消费量预测模型的预测结果更为稳定、准确，建模过程简单实用。
              </p>
              <p></p>
            </div>
          </div>
        </div>
      </div>
    </article>
    <!--网站尾部-->
    <footer></footer>
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.bootcss.com/echarts/3.8.5/echarts.min.js"></script>
    <script src="/static/charts.js"></script> <!-- charts render -->
    <script>
      /*<![CDATA[*/
      jQuery(document).ready(function () {
        $('#chart-type').change(function(){
          chart_refresh($('#chart-type').val());
        });
      
        $('#predict').click(function() {
          choose_mode('chart', $('#chart-type').val());
          predict_consumption($('#predict-type').val(), $('#gldxx').val(), $('#predict-term').val() === 'short' ? true : false, {{ .start_year }}, {{ .end_year }});
        });
      
        echarts.init(document.getElementById('chart')); // init chart
        $('#predict').click();
      });
      /*]]*/
    </script>
  </body>
</html>