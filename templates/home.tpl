<!DOCTYPE html>
<html lang="zh-hans">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="IE=Edge,chrome=1"/>
    <meta name="viewport" content="width=device-width"/>
    <title>数字化模拟粮食储备辅助决策系统</title>
    <link rel="stylesheet" href="/static/style.css"/>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <style>
      article > div.container {
      min-height: 70rem;
      }
      article > div.container > div.link-panel:not(:first-child) {
      margin-top: 3rem;
      }
      .link-panel {
      height: 140px;
      background-color: #fff;
      font-size: 5rem;
      border-radius: 5px;
      box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
      }
      .link-panel img {
      height: 130px;
      width: 130px;
      padding: 5px 5px 5px 5px;
      }
      .link-panel .content {
      text-align: center;
      }
    </style>
  </head>
  <body>
    <!--网站头部-->
    <header>
      <section id="header-banners">
        <section id="header-banner-title">
          <div class="container">
            <img alt="logo" src="/static/images/logo.png"/>
          </div>
        </section>
      </section>
      <nav>
        <div class="container">
          <ul>
            <li class="active">
              <a href="/">
              <img alt="home-icon" src="/static/images/icon-home.png"/> 首页
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/yield">
              <img alt="icon-price" src="/static/images/icon-dashboard.png"/> 产量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/consumption">
              <img alt="icon-price" src="/static/images/icon-price.png"/> 消费量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/reserves">
              <img alt="icon-price" src="/static/images/icon-search.png"/> 储备量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/admin">
              <img alt="icon-price" src="/static/images/icon-person.png"/> 后台管理
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!--主体部分-->
    <article>
      <div class="container">
        <div class="link-panel">
          <div class="content">
            <a href="/yield">
            <img alt="icon-price" src="/static/images/xiaomai.jpg"/> 产量预测
            </a>
          </div>
        </div>
        <div class="link-panel">
          <div class="content">
            <a href="/consumption">
            <img alt="icon-price" src="/static/images/rice.png"/> 消费量预测
            </a>
          </div>
        </div>
        <div class="link-panel">
          <div class="content">
            <a href="/reserves">
            <img alt="icon-price" src="/static/images/rice2.jpg"/> 储备量预测
            </a>
          </div>
        </div>
      </div>
    </article>
    <!--网站尾部-->
    <footer>
    </footer>
  </body>
</html>