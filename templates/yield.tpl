<!DOCTYPE html>
<html lang="zh-hans">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width" />
    <title>产量预测 - 数字化模拟粮食储备辅助决策系统</title>
    <link rel="stylesheet" href="/static/style.css" />
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  </head>
  <body>
    <!--网站头部-->
    <header>
      <section id="header-banners">
        <section id="header-banner-title">
          <div class="container">
            <img alt="logo" src="/static/images/logo.png" />
          </div>
        </section>
      </section>
      <nav>
        <div class="container">
          <ul>
            <li>
              <a href="/">
              <img alt="home-icon" src="/static/images/icon-home.png" /> 首页
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li class="active">
              <a href="/yield">
              <img alt="icon-price" src="/static/images/icon-dashboard.png" /> 产量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/consumption">
              <img alt="icon-price" src="/static/images/icon-price.png" /> 消费量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/reserves">
              <img alt="icon-price" src="/static/images/icon-search.png" /> 储备量预测
              </a>
            </li>
            <li class="header-nav-separator"></li>
            <li>
              <a href="/admin">
              <img alt="icon-price" src="/static/images/icon-person.png" /> 后台管理
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!--主体部分-->
    <article>
      <div class="container-fluid">
        <!-- 左侧说明框 -->
        <div class="col-md-2">
          <div class="panel option-panel">
            <div class="banner">
              <div class="banner-title">产量预测</div>
            </div>
            <div class="body" style="padding:0.1cm 0.2cm 0.1cm 0.2cm; height: 960px; overflow-y: scroll;">
              <h5><strong>目的及要求：</strong></h5>
              <p>分析研究现阶段我国粮食产量的变动规律及影响因素，探索适合我国粮食产量预测的方法，进一步提高预测精度和增强预测算法的鲁棒性；预测算法能够对我国及区域粮食主要品种进行中短期和中长期的预测。</p>
              <br/>
              <br/>
              <h5><strong>意义：</strong></h5>
              <p>粮食对于经济发展和社会稳定十分重要，我国是一个农业大国，粮食产量的多少、能否满足人民的需要是各级政府关注的主要问题之一。目前用于粮食产量预测的方法很多，主要有时间序列的回归方法、
                BP神经网络方法、灰度预测、多元线性回归预测等，由于粮食产量受到多种因素的影响，导致粮食产量时间序列的非平稳性；因此，分析研究现阶段我国粮食生产发展的变动规律，探索适合我国粮食产量预测的方法，
                进一步提高预测精度、增强预测算法的鲁棒性，不仅为制定粮食政策与实施粮食生产系统控制提供决策依据，而且对国家粮食安全具有重要的现实意义。
              </p>
            </div>
          </div>
        </div>
        <div class="container col-md-10">
          <div id="yield col-md-12">
            <div class="panel chart-panel col-md-9">
              <div class="banner">
                <div class="banner-title">
                  <span>产量预测</span>
                  <i class="banner-title-end"></i>
                </div>
              </div>
              <div class="body">
                <div class="chart" id="chart" style="height: 640px;"></div>
              </div>
            </div>
            <div class="panel option-panel col-md-3">
              <div class="banner">
                <div class="banner-title">选项</div>
              </div>
              <div class="body">
                <div class="options">
                  <ul>
                    <li>
                      <label>预测类型</label>
                      <select id="predict-type">
                        <option value="全国粮食">全国粮食</option>
                        <option value="全国稻谷">全国稻谷</option>
                        <option value="全国小麦" selected>全国小麦</option>
                        <option value="全国玉米">全国玉米</option>
                        <option disabled>---------</option>
                        <option value="河南粮食">河南粮食</option>
                        <option value="河南稻谷">河南稻谷</option>
                        <option value="河南小麦">河南小麦</option>
                        <option value="河南玉米">河南玉米</option>
                      </select>
                    </li>
                    <li>
                      <label>预测期限</label>
                      <select id="predict-term">
                        <option value="short" selected>中短期</option>
                        <option value="long">中长期</option>
                      </select>
                    </li>
                    <li>
                      <label>显示类型</label>
                      <select id="chart-type">
                        <option value="yield_data" selected>预测产量</option>
                        <option value="yield_if">影响因子</option>
                      </select>
                    </li>
                    <li>
                      <input id="predict" type="submit" value="预测" style="background: url(/static/images/button.png) 0 0 no-repeat; width: 75px; height: 30px; line-height: 30px; text-align: center; margin: 10px auto 0; color: #fff; cursor: pointer; border: 0 none;"  />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="panel option-panel col-md-12">
            <div class="banner">
              <div class="banner-title">模型详情</div>
            </div>
            <div class="body" style="height: 270px; overflow-y: scroll;">
              <h5><strong>产量预测模型特点：</strong></h5>
              <p>基于指数平滑及差分处理的粮食产量组合预测方法首先对影响粮食产量影响因素进行分析（粮食作物播种总面积、总劳动力、化肥总用量、有效灌溉面积、用电总量、机械总动力、受灾总面积等），
                对以往影响因子数据进行指数平滑处理，运用灰色理论迭代预测新的影响因子数据，然后通过影响因子关联度分析，自动选取关联度大的因子（预设一个阈值），最后对以往粮食产量数据进行差分处理以减小数据的波动，
                运用多元线性回归及残差修正预测未来粮食产量。该模型能够对全国及河南省主要粮食品种（粮食总产量、小麦、稻谷和玉米）进行中短期（5年）及中长期（10年）的预测，具有较好的鲁棒性和预测精度。
              </p>
              <h5><strong>基于指数平滑及差分处理的粮食产量组合预测模型的步骤：</strong></h5>
              <ol>
                <li>
                  <p>对往年影响粮食产量的因子数据进行差分运算计算影响因子波动量，根据波动量对往年影响因子数据进行平滑处理。</p>
                </li>
                <li>
                  <p>对步骤1处理过的数据进行指数平滑处理，进一步减小数据的波动。</p>
                </li>
                <li>
                  <p>运用灰色理论及处理过的往年影响粮食产量因子数据预测未来若干年的影响因子数据。</p>
                </li>
                <li>
                  <p>对往年粮食产量及影响因子数据进行归一化及平滑处理。</p>
                </li>
                <li>
                  <p>筛选关联系数大的影响因子。</p>
                </li>
                <li>
                  <p>运用多元回归进行预测并进行残差修正。</p>
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </article>
    <!--网站尾部-->
    <footer></footer>
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.bootcss.com/echarts/3.8.5/echarts.min.js"></script>
    <script src="/static/charts.js"></script> <!-- charts render -->
    <script>
      /*<![CDATA[*/
      $(document).ready(function () {
        $('#chart-type').change(function(){
          chart_refresh($('#chart-type').val());
        });
      
        $('#predict').click(function () {
          choose_mode('chart', $('#chart-type').val());
          predict_yield($('#predict-type').val(), $('#predict-term').val() === 'short', {{ .start_year }}, {{ .end_year }});
        });
      
        echarts.init(document.getElementById('chart')); // init chart
        $('#predict').click();
      });
      /*]]*/
    </script>
  </body>
</html>