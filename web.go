package main

import (
	"encoding/json"
	"html/template"
	"net/http"
	"regexp"
	"strconv"
)

// PageHome 主页
func PageHome(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("templates/home.tpl")
	t.Execute(w, nil)
}

// PageYield 产量预测模板渲染
func PageYield(w http.ResponseWriter, r *http.Request) {
	s := map[string]string{
		"start_year": "1978",
		"end_year":   "2015",
	}
	t, _ := template.ParseFiles("templates/yield.tpl")
	t.Execute(w, s)
}

// PageConsumption 消费量预测模板渲染
func PageConsumption(w http.ResponseWriter, r *http.Request) {
	s := map[string]string{
		"start_year": "1981",
		"end_year":   "2015",
	}
	t, _ := template.ParseFiles("templates/consumption.tpl")
	t.Execute(w, s)
}

// APIYield 产量预测 API
func APIYield(w http.ResponseWriter, r *http.Request) {
	// 解析提交的表单
	r.ParseForm()
	startYear, err := strconv.ParseInt(r.Form.Get("start_year"), 10, 32)
	if err != nil {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	endYear, err := strconv.ParseInt(r.Form.Get("end_year"), 10, 32)
	if err != nil {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	predictYear, err := strconv.ParseUint(r.Form.Get("predict_year"), 10, 32)
	if err != nil || predictYear == 0 {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	forecastType := r.Form.Get("type")
	match, _ := regexp.MatchString("^(?:全国|河南)(?:粮食|稻谷|小麦|玉米)$", forecastType)
	if !match {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}

	// 从 CSV 文件中读取数据
	ecData, haData := GetCSVTable(int(startYear), int(endYear))

	data := ForecastYield(
		GetColumn(ecData, 0),
		GetYieldECTable(ecData),
		GetYieldHATable(haData),
		forecastType,
		uint32(predictYear),
	)

	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	enc.Encode(data)
}

// APIConsumption 消费量预测 API
func APIConsumption(w http.ResponseWriter, r *http.Request) {
	// 解析提交的表单
	r.ParseForm()
	startYear, err := strconv.ParseInt(r.Form.Get("start_year"), 10, 32)
	if err != nil {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	endYear, err := strconv.ParseInt(r.Form.Get("end_year"), 10, 32)
	if err != nil {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	forecastPeriod, err := strconv.ParseUint(r.Form.Get("forecast_period"), 10, 32)
	if err != nil || forecastPeriod == 0 {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	rationalDegreeLowerLimit, err := strconv.ParseFloat(r.Form.Get("gldxx"), 64) // 关联度下限
	if err != nil {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}
	forecastType := r.Form.Get("type")
	match, _ := regexp.MatchString("^(?:全国|河南)$", forecastType)
	if !match {
		http.Error(w, "400 bad request", http.StatusBadRequest)
		return
	}

	ecData, haData := GetCSVTable(int(startYear), int(endYear))
	data := ForecastConsumption(
		GetColumn(ecData, 0),
		GetConsumptionECTable(ecData),
		GetConsumptionHATable(haData),
		uint32(forecastPeriod), // 预测期限
		forecastType,
		rationalDegreeLowerLimit, // 关联度下限
	)

	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	enc.Encode(data)
}
