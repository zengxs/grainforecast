package main

import (
	"math"
	"testing"
)

func TestForecastPrint(t *testing.T) {
	ForecastInit()
	defer ForecastTerm()

	mxDouble := mxCreateFromFloat(math.Pi)
	defer mxDestroy(mxDouble)

	mxPrint(mxDouble)
}
