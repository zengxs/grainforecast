package main

import (
	"encoding/csv"
	"io/ioutil"
	"strconv"
	"strings"
)

// StringTableToFloatTable 字符串表格转浮点数表格
func StringTableToFloatTable(stringTable [][]string) [][]float64 {
	var floatTable [][]float64
	for i := 0; i < len(stringTable); i++ {
		var row []float64
		for j := 0; j < len(stringTable[i]); j++ {
			number, _ := strconv.ParseFloat(stringTable[i][j], 64)
			row = append(row, number)
		}
		floatTable = append(floatTable, row)
	}
	return floatTable
}

// GetColumn 获取某一列数据
func GetColumn(dataTable [][]float64, column int32) []float64 {
	var data []float64
	for i := 0; i < len(dataTable); i++ {
		data = append(data, dataTable[i][column])
	}
	return data
}

// GetYieldECTable 从 CSV 表中获取 yield 预测需要的数据
//
// 并按其所要求的格式进行排序，如果 yield 函数修改了参数位置
// 或者 CSV 数据结构进行了更改，这里也必须随之改变
// TODO: 使用数据库替代 CSV
func GetYieldECTable(ecData [][]float64) [][]float64 {
	// 映射表
	var ecmap = []int32{1, 5, 9, 10, 11, 12, 13, 14, 2, 3, 4, 15, 18, 23, 19, 24, 6, 7, 8}
	var ecTable [][]float64
	for i := 0; i < len(ecmap); i++ { // 遍历 ecData 每一列
		ecTable = append(ecTable, GetColumn(ecData, ecmap[i]))
	}
	return ecTable
}

// GetYieldHATable 从 CSV 表中获取 yield 预测需要的数据
//
// 并按其所要求的格式进行排序，如果 yield 函数修改了参数位置
// 或者 CSV 数据结构进行了更改，这里也必须随之改变
// TODO: 使用数据库替代 CSV
func GetYieldHATable(haData [][]float64) [][]float64 {
	// 映射表
	var hamap = []int32{1, 6, 10, 11, 12, 13, 14, 15, 2, 3, 4, 5, 7, 8, 9}
	var haTable [][]float64
	for i := 0; i < len(hamap); i++ { // 遍历 ecData 每一列
		haTable = append(haTable, GetColumn(haData, hamap[i]))
	}
	return haTable
}

// GetConsumptionECTable 从 CSV 表中获取 consumption 预测需要的数据
//
// 并按其所要求的格式进行排序，如果 consumption 函数修改了参数位置
// 或者 CSV 数据结构进行了更改，这里也必须随之改变
// TODO: 使用数据库替代 CSV
func GetConsumptionECTable(ecData [][]float64) [][]float64 {
	// 映射表
	var ecmap = []int32{1, 21, 22, 18, 16, 19, 17, 20, 26, 27, 23, 24, 25}
	var ecTable [][]float64
	for i := 0; i < len(ecmap); i++ { // 遍历 ecData 每一列
		ecTable = append(ecTable, GetColumn(ecData, ecmap[i]))
	}
	return ecTable
}

// GetConsumptionHATable 从 CSV 表中获取 consumption 预测需要的数据
//
// 并按其所要求的格式进行排序，如果 consumption 函数修改了参数位置
// 或者 CSV 数据结构进行了更改，这里也必须随之改变
// TODO: 使用数据库替代 CSV
func GetConsumptionHATable(haData [][]float64) [][]float64 {
	// 映射表
	var hamap = []int32{1, 22, 23, 19, 17, 20, 18, 21, 27, 28, 24, 25, 26}
	var haTable [][]float64
	for i := 0; i < len(hamap); i++ { // 遍历 ecData 每一列
		haTable = append(haTable, GetColumn(haData, hamap[i]))
	}
	return haTable
}

// GetCSVTable 获取CSV Table
func GetCSVTable(startYear, endYear int) ([][]float64, [][]float64) {
	ec, _ := ioutil.ReadFile("data/ec.csv")
	ha, _ := ioutil.ReadFile("data/ha.csv")
	ecCsv := csv.NewReader(strings.NewReader(string(ec)))
	haCsv := csv.NewReader(strings.NewReader(string(ha)))
	_, _ = ecCsv.Read() // csv header
	_, _ = haCsv.Read() // csv header

	//  根据 startYear, endYear 过滤数据
	var ecStrTable, haStrTable [][]string
	for { // ecStrTable 处理
		ecRow, _ := ecCsv.Read()
		if ecRow == nil {
			break
		}
		year, _ := strconv.ParseInt(ecRow[0], 10, 32)
		if year >= int64(startYear) && year <= int64(endYear) {
			ecStrTable = append(ecStrTable, ecRow)
		}
	}
	for {
		haRow, _ := haCsv.Read()
		if haRow == nil {
			break
		}
		year, _ := strconv.ParseInt(haRow[0], 10, 32)
		if year >= int64(startYear) && year <= int64(endYear) {
			haStrTable = append(haStrTable, haRow)
		}
	}

	ecData := StringTableToFloatTable(ecStrTable)
	haData := StringTableToFloatTable(haStrTable)
	return ecData, haData
}
