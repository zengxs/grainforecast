package main

import (
	"net/http"
)

func main() {
	ForecastInit() // 初始化 MATLAB 环境

	// URL路由

	// 静态文件
	staticFileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", staticFileServer))

	//
	http.HandleFunc("/", PageHome)
	http.HandleFunc("/yield", PageYield)
	http.HandleFunc("/consumption", PageConsumption)

	// APIs
	http.HandleFunc("/api/forecast/yield", APIYield)
	http.HandleFunc("/api/forecast/consumption", APIConsumption)

	// 监听端口
	panic(http.ListenAndServe(":9090", nil))
}
