package main

/*
// 头文件搜索目录
#cgo CFLAGS: -I ./include -I /usr/lib/mcr711/extern/include

// 链接库搜索目录
// FIXME: 去除绝对路径依赖
#cgo LDFLAGS: -L ./lib -L /usr/lib/mcr711/runtime/glnxa64

// 需要链接的库
#cgo LDFLAGS: -lmwmclmcrrt
#cgo LDFLAGS: -lmlprint
#cgo LDFLAGS: -lyield
#cgo LDFLAGS: -lconsumption
#cgo LDFLAGS: -lreserves

#include "forecast.h"
*/
import "C"
import (
	"unsafe"
)

// ForecastInit 初始化 MATLAB 环境
func ForecastInit() {
	C.libmlprintInitialize()
	C.libyieldInitialize()
	C.libconsumptionInitialize()
	C.libreservesInitialize()
}

// ForecastTerm 清除 MATLAB 环境
func ForecastTerm() {
	C.libmlprintTerminate()
	C.libyieldTerminate()
	C.libconsumptionTerminate()
	C.libreservesTerminate()
}

func mxPrint(mxarray *C.mxArray) {
	C.mlprint(mxarray)
}

func mxCreateFromFloat(num float64) *C.mxArray {
	return C.mxCreateDoubleScalar(C.double(num))
}

func mxDestroy(mxarray *C.mxArray) {
	C.mxDestroyArray(mxarray)
}

// mxArrayToSlice 提取 mxArray 中的 (double) 数组到 Slice
func mxArrayToSlice(mxarray *C.mxArray) []float64 {
	m := uint32(C.mxGetM(mxarray))
	n := uint32(C.mxGetN(mxarray))
	ptr := C.mxGetPr(mxarray) // *C.double

	slice := make([]float64, m*n)
	for i := 0; i < len(slice); i++ {
		slice[i] = float64(C.get_double_from_array(ptr, C.size_t(i)))
	}

	return slice
}

// mxArrayToStringSlice 将包含多个字符串的 mxArray 转换成 go 字符串 slice
func mxArrayToStringSlice(mxarray *C.mxArray) []string {
	length := uint32(C.mxGetM(mxarray)) * uint32(C.mxGetN(mxarray))
	slice := make([]string, length)

	for i := uint32(0); i < length; i++ {
		// mxString 内存管理 (暂未搞清楚这里是否需要进行手动内存管理)
		// https://cn.mathworks.com/help/matlab/matlab_external/passing-strings-1.html#bumqpme
		cell := C.mxGetCell(mxarray, C.mwIndex(i))
		slice[i] = C.GoString(C.mxArrayToString(cell))
	}

	return slice
}

// ForecastYield 粮食产量预测
//
// TODO: 添加错误处理
// TODO: Godoc 格式的文档注释
//
// years: 年份 (float64 数组)
//
// ecTable: 全国数据表 (表中数据含义，按顺序)
// - 粮食总产量
// - 粮食作物播种总面积
// - 总劳动力
// - 化肥总用量
// - 有效灌溉面积
// - 用电总量
// - 机械总动力
// - 受灾总面积
// - 稻谷实际产量
// - 小麦实际产量
// - 玉米实际产量
// - 居民粮食消费量
// - 城镇人口
// - 农村人口
// - 城镇恩格尔系数
// - 农村恩格尔系数
// - 稻谷播种总面积
// - 小麦播种总面积
// - 玉米播种总面积
//
// haTable: 河南数据表 (表中数据含义，按顺序)
// - 粮食总产量
// - 粮食作物播种总面积
// - 总劳动力
// - 化肥总用量
// - 有效灌溉面积
// - 用电总量
// - 机械总动力
// - 受灾总面积
// - 稻谷实际产量
// - 小麦实际产量
// - 玉米实际产量
// - 农作物播种总面积
// - 稻谷播种总面积
// - 小麦播种总面积
// - 玉米播种总面积
func ForecastYield(
	years []float64, // 数据年份列表（原始数据）
	ecColumns [][]float64, // 全国数据
	haColumns [][]float64, // 河南数据
	forecastType string, // 预测类型
	forecastPeriod uint32, // 预测期限
) map[string]interface{} {
	// 用于选择预测类型
	detectForecastType := func(forecastType string) int32 {
		switch forecastType {
		case "全国粮食":
			return 10
		case "全国稻谷":
			return 11
		case "全国小麦":
			return 12
		case "全国玉米":
			return 13
		case "河南粮食":
			return 20
		case "河南稻谷":
			return 21
		case "河南小麦":
			return 22
		case "河南玉米":
			return 23
		default:
			return 0 // TODO: 返回一个错误
		}
	}

	// 预测类型
	forecastTypeNumber := C.mxCreateDoubleScalar(C.double(detectForecastType(forecastType)))
	defer C.mxDestroyArray(forecastTypeNumber)

	// 作为参数 m 传递给 MATLAB 函数
	dataLength := C.mxCreateDoubleScalar(C.double(len(years)))
	defer C.mxDestroyArray(dataLength)

	// 作为参数 m 传递给 MATLAB 函数
	mxForecastPeriod := C.mxCreateDoubleScalar(C.double(forecastPeriod))
	defer C.mxDestroyArray(mxForecastPeriod)

	// 根据年份数据创建 mxArray
	mxYears := C.forecastCreateArray((*C.double)(unsafe.Pointer(&years[0])), C.size_t(len(years)))
	defer C.mxDestroyArray(mxYears)

	// 根据 ecTable 创建 mxArray 列表
	var ecArray []*C.mxArray
	for i := 0; i < len(ecColumns); i++ {
		slice := ecColumns[i]
		// 创建 mxArray
		array := C.forecastCreateArray((*C.double)(unsafe.Pointer(&slice[0])), C.size_t(len(slice)))
		// 退出函数前释放该对象
		defer C.mxDestroyArray(array)
		ecArray = append(ecArray, array)
	}

	// 根据 haTable 创建 mxArray 列表
	var haArray []*C.mxArray
	for i := 0; i < len(haColumns); i++ {
		slice := haColumns[i]
		// 创建 mxArray
		array := C.forecastCreateArray((*C.double)(unsafe.Pointer(&slice[0])), C.size_t(len(slice)))
		// 退出函数前释放该对象
		defer C.mxDestroyArray(array)
		haArray = append(haArray, array)
	}

	// 用于保存 MATLAB 函数输出的数据
	var outputPtr [12]*C.mxArray
	// 调用 MATLAB 函数
	C.mlfLiangShiChanLiangYuCe(12,
		// ===== 输出数据 =====
		&outputPtr[0],
		&outputPtr[1],
		&outputPtr[2],
		&outputPtr[3],
		&outputPtr[4],
		&outputPtr[5],
		&outputPtr[6],
		&outputPtr[7],
		&outputPtr[8],
		&outputPtr[9],
		&outputPtr[10],
		&outputPtr[11],
		// ===== 输入参数 =====
		mxYears, // 年份
		// 全国数据
		ecArray[0],  // 粮食总产量
		ecArray[1],  // 粮食作物播种总面积
		ecArray[2],  // 总劳动力
		ecArray[3],  // 化肥总用量
		ecArray[4],  // 有效灌溉面积
		ecArray[5],  // 用电总量
		ecArray[6],  // 机械总动力
		ecArray[7],  // 受灾总面积
		ecArray[8],  // 稻谷实际产量
		ecArray[9],  // 小麦实际产量
		ecArray[10], // 玉米实际产量
		ecArray[11], // 居民粮食消费量
		ecArray[12], // 城镇人口
		ecArray[13], // 农村人口
		ecArray[14], // 城镇恩格尔系数
		ecArray[15], // 农村恩格尔系数
		ecArray[16], // 稻谷播种总面积
		ecArray[17], // 小麦播种总面积
		ecArray[18], // 玉米播种总面积
		// 河南数据
		haArray[0],  // 粮食总产量
		haArray[1],  // 粮食作物播种总面积
		haArray[2],  // 总劳动力
		haArray[3],  // 化肥总用量
		haArray[4],  // 有效灌溉面积
		haArray[5],  // 用电总量
		haArray[6],  // 机械总动力
		haArray[7],  // 受灾总面积
		haArray[8],  // 稻谷实际产量
		haArray[9],  // 小麦实际产量
		haArray[10], // 玉米实际产量
		haArray[11], // 农作物播种总面积
		haArray[12], // 稻谷播种总面积
		haArray[13], // 小麦播种总面积
		haArray[14], // 玉米播种总面积
		// 其他数据
		forecastTypeNumber, // 预测类型
		dataLength,         // 数据量
		mxForecastPeriod,   // 预测期限
	)

	// TODO: 将 output 转换成可返回的数据
	outputData := make(map[string]interface{}) // 用于储存任意类型的数据

	outputData["year"] = mxArrayToSlice(outputPtr[0])
	outputData["di_afs"] = mxArrayToSlice(outputPtr[3])    // 产量
	outputData["di_afs_em"] = mxArrayToSlice(outputPtr[4]) // 产量修正
	outputData["关联度"] = mxArrayToSlice(outputPtr[6])

	// 关联系数名称 (字符串列表，按顺序与 outputPtr[7] 中的数据一一对应)
	glxsName := mxArrayToStringSlice(outputPtr[8])
	glxs := make(map[string][]float64)

	// 从 outputPtr[7] 中提取数据
	// outputPtr[7] 是一个矩阵，每一列分别与 glxsName 中的名称对应
	// 但是 outputPtr[7] 返回的是一个多行的数据，我们只需要其中最后几行
	// (所需行数与 forecastPeriod 相同)
	getGlxsData := func(mxarray *C.mxArray, neededLength uint32) [][]float64 {
		m := uint32(C.mxGetM(mxarray))
		n := uint32(C.mxGetN(mxarray))
		pr := C.mxGetPr(mxarray)

		var data [][]float64
		for i := uint32(0); i < n; i++ {
			var row []float64
			for j := uint32(0); j < m; j++ {
				offset := m*i + j
				number := C.get_double_from_array(pr, C.size_t(offset))
				row = append(row, float64(number))
			}
			// 截取最后 neededLength 个元素
			row = row[uint32(len(row))-1-neededLength:]
			data = append(data, row)
		}
		return data
	}
	glxsData := getGlxsData(outputPtr[7], forecastPeriod)
	for i := 0; i < len(glxsName); i++ {
		glxs[glxsName[i]] = glxsData[i]
	}
	outputData["关联系数"] = glxs

	// 销毁 output
	for i := 0; i < len(outputPtr); i++ {
		C.mxDestroyArray(outputPtr[i])
	}
	return outputData
}

// ForecastConsumption 粮食消费量预测
// - 年份
// ecColumns: 全国数据表 (表中数据含义，按顺序)
// - 粮食总产量
// - 城镇口粮消费量
// - 城镇饲料粮消费量
// - 城镇人口
// - 城镇化水平(城镇化率/%)
// - 城镇恩格尔系数
// - 农产品生产价格指数
// - 城镇人均收入
// - 农村口粮消费量
// - 农村饲料粮消费量
// - 农村人口
// - 农村恩格尔系数
// - 农村人均收入
// haColumns: 河南数据表
// - 粮食总产量
// - 城镇口粮消费量
// - 城镇饲料粮消费量
// - 城镇人口
// - 城镇化水平(城镇化率/%)
// - 城镇恩格尔系数
// - 农产品生产价格指数
// - 城镇人均收入
// - 农村口粮消费量
// - 农村饲料粮消费量
// - 农村人口
// - 农村恩格尔系数
// - 农村人均收入
func ForecastConsumption(
	years []float64, // 数据年份列表（原始数据）
	ecColumns [][]float64, // 全国数据
	haColumns [][]float64, // 河南数据
	forecastPeriod uint32, // 预测期限
	forecastType string, // 预测内容：全国/河南
	rationalDegreeLowerLimit float64, // 关联度下限
) map[string]interface{} {
	const M int32 = 80 // M=80

	detectForecastType := func(forecastType string) int32 {
		switch forecastType {
		case "全国":
			return 10
		case "河南":
			return 20
		default:
			return 0 // TODO: 返回一个错误
		}
	}

	// ===== 参数转换 =====

	// 预测类型
	mxForecastTypeNumber := C.mxCreateDoubleScalar(C.double(detectForecastType(forecastType)))
	defer C.mxDestroyArray(mxForecastTypeNumber)

	// mxM
	mxM := C.mxCreateDoubleScalar(C.double(M))
	defer C.mxDestroyArray(mxM)

	// mxRationalDegreeLowerLimit
	mxRationalDegreeLowerLimit := C.mxCreateDoubleScalar(C.double(rationalDegreeLowerLimit))
	defer C.mxDestroyArray(mxRationalDegreeLowerLimit)

	// 预测期限
	mxForecastPeriod := C.mxCreateDoubleScalar(C.double(forecastPeriod))
	defer C.mxDestroyArray(mxForecastPeriod)

	// 根据年份数据创建 mxArray
	mxYears := C.forecastCreateArray((*C.double)(unsafe.Pointer(&years[0])), C.size_t(len(years)))
	defer C.mxDestroyArray(mxYears)

	// 根据 ecColumns 创建 mxArray 列表
	var ecArray []*C.mxArray
	for i := 0; i < len(ecColumns); i++ {
		slice := ecColumns[i]
		// 创建 mxArray
		array := C.forecastCreateArray((*C.double)(unsafe.Pointer(&slice[0])), C.size_t(len(slice)))
		// 退出函数前释放该对象
		defer C.mxDestroyArray(array)
		ecArray = append(ecArray, array)
	}

	var haArray []*C.mxArray
	for i := 0; i < len(haColumns); i++ {
		slice := haColumns[i]
		// 创建 mxArray
		array := C.forecastCreateArray((*C.double)(unsafe.Pointer(&slice[0])), C.size_t(len(slice)))
		// 退出函数前释放该对象
		defer C.mxDestroyArray(array)
		haArray = append(haArray, array)
	}

	// 开始预测
	var outputPtr [20]*C.mxArray

	C.mlfLiangshizongxiaofeiliangyuce(20,
		&outputPtr[0],  // 年份
		&outputPtr[1],  // M
		&outputPtr[2],  // 总消费量
		&outputPtr[3],  // 工业用粮
		&outputPtr[4],  // 损耗用粮
		&outputPtr[5],  // 种子用粮
		&outputPtr[6],  // 口粮总消费量
		&outputPtr[7],  // 饲料粮总消费量
		&outputPtr[8],  // 城镇口粮消费量
		&outputPtr[9],  // 城镇饲料粮消费量
		&outputPtr[10], // 农村口粮消费量
		&outputPtr[11], // 农村饲料粮消费量
		&outputPtr[12], // 关联系数1
		&outputPtr[13], // 关联系数2
		&outputPtr[14], // 关联系数3
		&outputPtr[15], // 关联系数4
		&outputPtr[16], // 关联系数名称1
		&outputPtr[17], // 关联系数名称2
		&outputPtr[18], // 关联系数名称3
		&outputPtr[19], // 关联系数名称4
		// ===== 输入参数 =====
		mxYears,                    // 年份
		mxRationalDegreeLowerLimit, // GuanLianDu_Yuzhi
		mxM,              //   M=80
		mxForecastPeriod, // N
		mxForecastTypeNumber,
		// 全国数据
		ecArray[0],  // - 粮食总产量
		ecArray[1],  // - 城镇口粮消费量
		ecArray[2],  // - 城镇饲料粮消费量
		ecArray[3],  // - 城镇人口
		ecArray[4],  // - 城镇化水平(城镇化率/%)
		ecArray[5],  // - 城镇恩格尔系数
		ecArray[6],  // - 农产品生产价格指数
		ecArray[7],  // - 城镇人均收入
		ecArray[8],  // - 农村口粮消费量
		ecArray[9],  // - 农村饲料粮消费量
		ecArray[10], // - 农村人口
		ecArray[11], // - 农村恩格尔系数
		ecArray[12], // - 农村人均收入
		// 粮食总产量
		haArray[0],  // - 粮食总产量
		haArray[1],  // - 城镇口粮消费量
		haArray[2],  // - 城镇饲料粮消费量
		haArray[3],  // - 城镇人口
		haArray[4],  // - 城镇化水平(城镇化率/%)
		haArray[5],  // - 城镇恩格尔系数
		haArray[6],  // - 农产品生产价格指数
		haArray[7],  // - 城镇人均收入
		haArray[8],  // - 农村口粮消费量
		haArray[9],  // - 农村饲料粮消费量
		haArray[10], // - 农村人口
		haArray[11], // - 农村恩格尔系数
		haArray[12], // - 农村人均收入
	)

	// ===== 提取数据 =====
	output := make(map[string]interface{})

	output["year"] = mxArrayToSlice(outputPtr[0])
	output["total"] = mxArrayToSlice(outputPtr[2])    // 粮食总消费量
	output["total_gy"] = mxArrayToSlice(outputPtr[3]) // 工业用粮
	output["total_sh"] = mxArrayToSlice(outputPtr[4]) // 损耗用粮
	output["total_zz"] = mxArrayToSlice(outputPtr[5]) // 种子用粮
	output["total_kl"] = mxArrayToSlice(outputPtr[6]) // 口粮消费量
	output["total_sl"] = mxArrayToSlice(outputPtr[7]) // 饲料粮消费量
	output["cz_kl"] = mxArrayToSlice(outputPtr[8])    // 城镇口粮消费量
	output["cz_sl"] = mxArrayToSlice(outputPtr[9])    // 城镇饲料粮消费量
	output["nc_kl"] = mxArrayToSlice(outputPtr[10])   // 农村口粮消费量
	output["nc_sl"] = mxArrayToSlice(outputPtr[11])   // 农村饲料粮消费量

	// ===== 销毁 mxArray =====
	for i := 0; i < len(outputPtr); i++ {
		C.mxDestroyArray(outputPtr[i])
	}

	return output
}
