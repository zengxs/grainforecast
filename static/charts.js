var localStore = null;
var localChartId = null;
var localChartName = null;

function choose_mode(chart_id, chart_name) {
  localChartId = chart_id;
  localChartName = chart_name;
}

function check_mode() {
  if (!localChartId || !localChartName)
    alert("JS错误: 尚未选择 Chart Dom ID 或预测模型。");
}

// 保存数据
// 预测后将数据保存在 localStore, 然后手动调用 chart_refresh 刷新图表
function save_data(data) { localStore = data; }

// ajax 错误调用此函数
function ajax_error_handle(xhr, error_text) {
  alert('无法获取预测数据:\n\n' +
        '\t请求状态: ' + xhr.readyState + '\n' +
        '\t服务端响应: ' + xhr.status + '\n' +
        '\t错误详情:' + xhr.responseText);
}

// 请求 yield 的预测数据
// |:- param -|:- type -|:- require -:|
// model, String, "要显示的模型名称"
// short_term, Bool, "中短期(true) / 中长期(false)"
function predict_yield(model, short_term, start_year, end_year) {
  // localChartName: yield_data(预测数据, 产量), yield_if(影响因子, impact
  // factor)
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();
  instance.showLoading();

  var payload = {
    start_year : start_year,
    end_year : end_year,
    predict_year : short_term ? 5 : 10,
    type : model
  };
  jQuery.ajax({
    type : 'POST',
    url : '/api/forecast/yield',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

// 请求 consumption 的预测数据
// @param n   预测的年数
// @param gldxx 关联度下限
function predict_consumption(type, gldxx, short_term, start_year, end_year) {
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();
  instance.showLoading();

  var payload = {
    forecast_period : short_term ? 5 : 10,
    start_year : start_year,
    end_year : end_year,
    type: type,
    gldxx : gldxx,
  };
  jQuery.ajax({
    type : 'POST',
    url : '/api/forecast/consumption',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

// 请求 reserves 的预测数据

// bdsx, Number, "波动指数上限"
// bdxx, Number, "波动指数下限"
function predict_reserves_bdzsvs(short_term, bdsx, bdxx, start_year, end_year) {
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();       // 清除原有图表
  instance.showLoading(); // 显示 loading 动画
  var payload = {
    start_year : start_year,
    end_year : end_year,
    m : 40,
    n : short_term ? 5 : 10,
    bdsx : bdsx,
    bdxx : bdxx,
  };
  jQuery.ajax({
    type : 'POST',
    url : '/predict/reserves/bdzs',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

// aqxs: 安全系数
function predict_reserves_qeceblmxxz(short_term, aqxs, start_year, end_year) {
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();       // 清除原有图表
  instance.showLoading(); // 显示 loading 动画
  var payload = {
    start_year : start_year,
    end_year : end_year,
    n : short_term ? 5 : 10,
    m : 40,
    aqxs : aqxs,
  };
  jQuery.ajax({
    type : 'POST',
    url : '/predict/reserves/qeceblmxxz',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

function predict_reserves_jjsszx(short_term, start_year, end_year) {
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();       // 清除原有图表
  instance.showLoading(); // 显示 loading 动画
  var payload = {
    start_year : start_year,
    end_year : end_year,
    n : short_term ? 5 : 10,
    m : 40,
  };
  jQuery.ajax({
    type : 'POST',
    url : '/predict/reserves/jjsszx',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

function predict_reserves_lsaq(short_term, start_year, end_year) {
  check_mode();
  var instance = echarts.getInstanceByDom($('#' + localChartId)[0]);
  instance.clear();       // 清除原有图表
  instance.showLoading(); // 显示 loading 动画
  var payload = {
    start_year : start_year,
    end_year : end_year,
    n : short_term ? 5 : 10,
    m : 40,
  };
  jQuery.ajax({
    type : 'POST',
    url : '/predict/reserves/lsaq',
    data : payload,
    dataType : 'json',
    error : ajax_error_handle,
    success : function(data) {
      save_data(data);
      chart_refresh(localChartName);
    }
  });
}

// 刷新模型的解释与特点
function detail_refresh(model_name, explain_id, feature_id) {
  switch (model_name) {
  // reserves models
  case 'bdzsvs':
    $('#' + explain_id)
        .html(
            '<strong>基于粮食产量的波动指数法</strong>' +
            '<p>波动指数法，首先计算出各年粮食产量的波动指数，再结合预先设定的粮食可接受' +
            '波动上下限，确定出各年的吞吐量。然后计算出累计吞入量与吐出量，通过比较分析' +
            '最终确定合适的储备规模，可接受波动程度的不同，储备规模也因此而不同。</p>' +
            '<strong>基于粮食需求的波动指数法</strong>' +
            '<p>首先计算出各年粮食需求的波动指数，再结合预先设定的可接受波动上下限并考虑粮食需求量的变化' +
            '趋势，确定出各年的吞吐量。然后计算出累计吞入量与吐出量，通过比较分析最终确定合适的储备规模，' +
            '可接受波动程度的不同，储备规模也因此而不同。</p>');
    $('#' + feature_id)
        .html(
            '<strong>基于粮食产量的波动指数法</strong>' +
            '<p>模型建立过程简单，依据粮食产量每年的波动值、波动指数以及预先设定的波动范围计算出各年为熨平' +
            '粮食产量波动而应吞入量或吐出量。但模型没有考虑市场对粮食的需求变化情况。</p>' +
            '<strong>基于粮食需求的波动指数法</strong>' +
            '<p>模型建立过程简单，依据粮食需求每年的波动值、波动指数、预先设定的波动范围以及粮食需求的变化' +
            '趋势计算出各年为熨平粮食需求波动而应吞入量或吐出量。</p>');
    break;
  case 'qeceblmxxz':
    $('#' + explain_id)
        .html(
            '<strong>全额模型</strong>' +
            '<p>当某年度的粮食需求波动超过某设定的可容忍波动幅度时，就用专项储备予以调节，吞吐量等于实际粮食需求与需求量趋势的差额，当某年度的需求波动在设定的可容忍波动幅度范围以内时，就由市场和其他方式予以调节，不动用专项储备。</p>' +
            '<strong>差额模型</strong>' +
            '<p>当粮食需求的波动幅度超过可容忍的限度时，计算实际粮食需求与需求量趋势的差额，只对超过波动幅度的部分进行差额调整。差额模型与全额模型的差别在于对超过可容忍幅度的粮食需求波动调节的数量不同。</p>' +
            '<strong>比例模型</strong>' +
            '<p>改进比例模型是对改进全额模型的一种变形，在比例模型下，不管该年度产量波动的大小，都进行同一比例的调整。这样既可以减轻需求波动的冲击，又克服了全额模型下突变式处理办法的弊端。其与全额模型以及差额模型的实质性区别在于对超过可容忍限度的粮食需求波动的调节数量的不同。</p>');
    $('#' + feature_id)
        .html(
            '<strong>全额模型</strong>' +
            '<p>对粮食安全界限两侧附近波动的调整突变性太大，只要实际粮食需求仅仅超过可容忍界限一点点，就动用粮食储备进行调节到趋势产量，而只要粮食需求波动范围不超出可容忍界限就不动用粮食储备进行调节。</p>' +
            '<strong>差额模型</strong>' +
            '<p>在同样的粮食安全水平下，利用差额模型推算的粮食储备规模相对较小，也就是说在差额模型的情况下，可以用较少的粮食储备达到较高的粮食安全水平。</p>' +
            '<strong>比例模型</strong>' +
            '<p>针对所有波动按一定比例进行调整，但是调整的目标数量是不一致的，最大粮食需求波动年份调整至粮食安全目标所容忍的边界线上，而其他年份调整的目的数量就参差不齐。</p>');
    break;
  case 'jjsszx':
    $('#' + explain_id)
        .text(
            '该方法在综合考虑粮食生产、粮食进口、粮食消费和粮食储备关系的基础上，首先根据粮食生产和进口的历年数据建立粮食供给的预测模型，然后计算历年口粮消费总量并建立口粮消费模型。' +
            '针对粮食总消费量的不连续性、非统计性和复杂性的特点，计算几个离散年份的粮食总消费量，并计算口粮消费在这些年份中所占的比例，进而构建比例模型。依据口粮消费模型和比例模型得' +
            '到粮食总消费量模型。在上述模型的基础上，根据粮食供需关系，构建粮食短缺经济损失函数和粮食储备成本函数，在保证经济损失最小的情况下，建立最优粮食储备模型。');
    $('#' + feature_id)
        .text(
            '在同时考虑粮食短缺时造成的经济损失和粮食储藏时需要的储备成本的前提下，综合考虑粮食生产、粮食进口、粮食消费、粮食储藏的单位成本和粮食短缺的单位经济损失等各种因素对经济的' +
            '影响，从而得到最优粮食储藏模型。因此，本模型更贴近我国粮食实际储藏情况、更能发挥粮食储藏的经济作用，同时该模型计算简单、实用、客观。');
    break;
  case 'lsaq':
    $('#' + explain_id)
        .text(
            '该模型以粮食供应出现不足时保证由此引起的经济损失最小为优化目标，将粮食供应来源分为国内自给与粮食进口两个部分，在确定两部分所占比重、粮食供应中断几率、单位粮食短缺引起的' +
            '经济损失以及单位粮食储藏成本的基础上，针对可能出现的四种粮食供给中断的发生概率和由此造成的经济损失，利用多目标优化方法分别建立了无粮食储藏和具有粮食储藏两种情况下粮食短' +
            '缺引起的总的经济损失量，进而根据粮食储藏情况下粮食供应波动引起的经济损失最小的原则，建立了最优粮食储藏模型。');
    $('#' + feature_id)
        .text(
            '模型在同时考虑粮食短缺时造成的经济损失和粮食储藏时需要的储备成本的前提下，综合考虑粮食自给率、进口率、粮食自给不足风险、粮食进口中断风险、粮食储藏的单位成本、粮食短缺的' +
            '单位经济损失和粮食供给量等各种因素对经济的影响，从而得到最优粮食储藏模型。因此，模型考虑的因素更多、更贴近我国粮食实际储藏情况、更能发挥粮食储藏的粮食安全保障作用。');
    break;
  }
}

//
function options_refresh(model_name, container_id) {
  var predict_term_option =
      '<li>' +
      '&nbsp;<label>预测期限</label>' +
      '&nbsp;<select id="predict-term">' +
      '&nbsp;&nbsp;<option value="short" selected>中短期</option>' +
      '&nbsp;&nbsp;<option value="long">中长期</option>' +
      '&nbsp;</select>' +
      '</li>\n';
  switch (model_name) {
  // reserves models
  case 'bdzsvs':
    $('#' + container_id)
        .html(
            predict_term_option +
            '<li><label>波动指数上限</label><input id="bdsx" type="text" value="2"/></li><br/>' +
            '<li><label>波动指数下限</label><input id="bdxx" type="text" value="-2"/></li><br/>');
    break;
  case 'qeceblmxxz':
    $('#' + container_id)
        .html(
            predict_term_option +
            '<li><label>安全系数</label><input id="aqxs" type="text" value="0.98"/></li><br/>');
    break;
  case 'jjsszx':
  case 'lsaq':
    $('#' + container_id).html(predict_term_option);
    break;
  }
}

// 刷新 chart, 调用此函数前，如需更改预测参数，需要重新请求预测数据
function chart_refresh(chart_name) {
  // check data
  if (localStore === null) {
    alert("没有缓存的预测数据，请重新预测！");
    return;
  }

  localChartName = chart_name; // 缓存预测模型名字

  var instance =
      echarts.getInstanceByDom(document.getElementById(localChartId));
  instance.clear();       // 清除原有图表
  instance.showLoading(); // 显示 loading 动画

  switch (localChartName) {
  // 产量模型
  case 'yield_data': // 预测数据, 产量
    refresh_yield_data_chart(localChartId, localStore);
    break;
  case 'yield_if': // 影响因子, impact factor
    refresh_yield_if_chart(localChartId, localStore);
    break;

  // 消费量模型
  case 'consumption':
    refresh_consumption_chart(localChartId, localStore);
    break;
  case 'consumption_glxs_ck': // 关联系数：城镇口粮
    refresh_consumption_glxs_ck_chart(localChartId, localStore);
    break;
  case 'consumption_glxs_cs': // 关联系数：城镇饲料粮
    refresh_consumption_glxs_cs_chart(localChartId, localStore);
    break;
  case 'consumption_glxs_xk': // 关联系数：乡村口粮
    refresh_consumption_glxs_xk_chart(localChartId, localStore);
    break;
  case 'consumption_glxs_xs': // 关联系数：乡村饲料粮
    refresh_consumption_glxs_xs_chart(localChartId, localStore);
    break;
  case 'consumption_czt': // 城镇总用粮
    refresh_consumption_czt(localChartId, localStore);
    break;
  case 'consumption_xct': // 乡村总用粮
    refresh_consumption_xct(localChartId, localStore);
    break;
  case 'consumption_gyyl': // 工业用粮
    refresh_consumption_gyyl(localChartId, localStore);
    break;
  case 'consumption_sh': // 损耗
    refresh_consumption_sh(localChartId, localStore);
    break;
  case 'consumption_zz': // 种子
    refresh_consumption_zz(localChartId, localStore);
    break;

  // 储备量模型
  case 'bdzsvs': // 全额、差额、比例模型
    refresh_bdzsvs_chart(localChartId, localStore);
    break;
  case 'qeceblmxxz': // 基于消费的全额、差额、比例模型
    refresh_qeceblmxxz_chart(localChartId, localStore);
    break;
  case 'jjsszx': // 经济损失最小模型
    refresh_jjsszx_chart(localChartId, localStore);
    break;
  case 'lsaq': // 粮食安全模型
    refresh_lsaq_chart(localChartId, localStore);
    break;
  }
}

// 产量: 预测数据, 产量
function refresh_yield_data_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ 'DI-AFS 多元预测', 'DI-AFS-EM 多元预测' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : 'DI-AFS 多元预测', type : 'bar', data : data.di_afs},
      {name : 'DI-AFS-EM 多元预测', type : 'bar', data : data.di_afs_em},
    ]
  });
  instance.hideLoading();
}

// 产量: 影响因子, impact factor
function refresh_yield_if_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));

  column_names = [];
  chart_series = [];
  for (影响因子 in data.关联系数) {
    column_names.push(影响因子);
    chart_series.push(
        {name : 影响因子, type : 'bar', data : data.关联系数[影响因子]});
  }

  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : column_names},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [
      {type : 'value', axisLabel : {formatter : '{value}'}, scale : false}
    ],
    series : chart_series
  });
  instance.hideLoading();
}

function refresh_consumption_chart(chart_id, data) {
  // 重新初始化，防止递归造成的浏览器崩溃
  // FIXME: 多次切换饼形图时高CPU占用，页面短暂卡死，响应时间过长
  var instance = echarts.init(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '粮食消费量' ]},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '粮食消费量', type : 'bar', data : data.total},
    ]
  });
  instance.hideLoading();
  // 点击事件
  instance.on('click', function(params) {
    instance.clear();
    instance.showLoading();
    instance.setOption({
      title : {
        text : params.name + '年 ' + params.seriesName + ' 预测消费量组成',
        x : 'center',
      },
      tooltip : {trigger : 'item', formatter : '{b} : {c} 万吨 ({d}%)'},
      legend : {
        orient : 'vertical',
        left : 'left',
        data : [ '口粮', '饲料粮', '工业用粮', '种子用粮', '损耗' ]
      },
      series : [ {
        type : 'pie',
        radius : '70%',
        center : [ '50%', '55%' ],
        data : [
          {value : data.total_kl[params.dataIndex], name : '口粮'},
          {value : data.total_sl[params.dataIndex], name : '饲料粮'},
          {value : data.total_gy[params.dataIndex], name : '工业用粮'},
          {value : data.total_zz[params.dataIndex], name : '种子用粮'},
          {value : data.total_sh[params.dataIndex], name : '损耗'},
        ],
        itemStyle : {
          emphasis : {
            shadowBlur : 10,
            shadowOffsetX : 0,
            shadowColor : 'rgba(0, 0, 0, 0.5)',
          }
        }
      } ]
    });
    instance.hideLoading();
    // 双击恢复原图形
    instance.on('click', function(params) { chart_refresh('consumption'); });
  });
}

// 关联系数：城镇口粮
function refresh_consumption_glxs_ck_chart(chart_id, data) {
  var instance = echarts.init(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {
      data : [ '城镇人口', '城镇化水平', '恩格尔系数', '农产品生产价格指数' ]
    },
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year_all
    } ],
    yAxis : [
      {type : 'value', axisLabel : {formatter : '{value}'}, scale : false}
    ],
    series : [
      {name : '城镇人口', type : 'bar', data : data.huise_glxs_ck[0]},
      {name : '城镇化水平', type : 'bar', data : data.huise_glxs_ck[1]},
      {name : '恩格尔系数', type : 'bar', data : data.huise_glxs_ck[2]}, {
        name : '农产品生产价格指数',
        type : 'bar',
        data : data.huise_glxs_ck[3]
      }
    ]
  });
  instance.hideLoading();
}

// 关联系数：城镇饲料粮
function refresh_consumption_glxs_cs_chart(chart_id, data) {
  var instance = echarts.init(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '城镇人口', '城镇化水平', '城镇人均收入' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year_all
    } ],
    yAxis : [
      {type : 'value', axisLabel : {formatter : '{value}'}, scale : false}
    ],
    series : [
      {name : '城镇人口', type : 'bar', data : data.huise_glxs_cs[0]},
      {name : '城镇化水平', type : 'bar', data : data.huise_glxs_cs[1]},
      {name : '城镇人均收入', type : 'bar', data : data.huise_glxs_cs[2]}
    ]
  });
  instance.hideLoading();
}

// 关联系数：乡村口粮
function refresh_consumption_glxs_xk_chart(chart_id, data) {
  var instance = echarts.init(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {
      data : [ '乡村人口', '城镇化水平', '恩格尔系数', '农产品生产价格指数' ]
    },
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year_all
    } ],
    yAxis : [
      {type : 'value', axisLabel : {formatter : '{value}'}, scale : false}
    ],
    series : [
      {name : '乡村人口', type : 'bar', data : data.huise_glxs_xk[0]},
      {name : '城镇化水平', type : 'bar', data : data.huise_glxs_xk[1]},
      {name : '恩格尔系数', type : 'bar', data : data.huise_glxs_xk[2]}, {
        name : '农产品生产价格指数',
        type : 'bar',
        data : data.huise_glxs_ck[3]
      }
    ]
  });
  instance.hideLoading();
}

// 关联系数：乡村口粮
function refresh_consumption_glxs_xs_chart(chart_id, data) {
  var instance = echarts.init(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '乡村人口', '城镇化水平', '乡村人均收入' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year_all
    } ],
    yAxis : [
      {type : 'value', axisLabel : {formatter : '{value}'}, scale : false}
    ],
    series : [
      {name : '乡村人口', type : 'bar', data : data.huise_glxs_xs[0]},
      {name : '城镇化水平', type : 'bar', data : data.huise_glxs_xs[1]},
      {name : '乡村人均收入', type : 'bar', data : data.huise_glxs_xs[2]}
    ]
  });
  instance.hideLoading();
}

// 消费量：口粮
function refresh_consumption_czt(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '口粮消费量' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year // 取与 mx31 相同大小的横坐标(即年份)
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '口粮消费量', type : 'bar', data : data.total_kl},
    ]
  });
  instance.hideLoading();
}

// 消费量：饲料粮
function refresh_consumption_xct(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '饲料粮消费量' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year // 取与 mx31 相同大小的横坐标(即年份)
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '饲料粮消费量', type : 'bar', data : data.total_sl},
    ]
  });
  instance.hideLoading();
}

// 消费量：工业用粮
function refresh_consumption_gyyl(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '工业用粮' ]},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year // 取与 mx31 相同大小的横坐标(即年份)
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '工业用粮', type : 'bar', data : data.total_gy},
    ]
  });
  instance.hideLoading();
}

// 消费量：损耗
function refresh_consumption_sh(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '损耗用粮' ]},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year // 取与 mx31 相同大小的横坐标(即年份)
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '损耗用粮', type : 'bar', data : data.total_sh},
    ]
  });
  instance.hideLoading();
}

// 消费量：种子
function refresh_consumption_zz(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '种子用粮' ]},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year // 取与 mx31 相同大小的横坐标(即年份)
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '种子用粮', type : 'bar', data : data.total_zz},
    ]
  });
  instance.hideLoading();
}

// 刷新图表: 波动指数粮食储备粮模型 vs 基于粮食消费的波动指数法粮食储备量预测
function refresh_bdzsvs_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '波动指数法', '改进波动指数法' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '波动指数法', type : 'bar', data : data.mx1},
      {name : '改进波动指数法', type : 'bar', data : data.mx2},
    ]
  });
  instance.hideLoading();
}

// 刷新图表: 基于消费的全额、差额、比例模型
function refresh_qeceblmxxz_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '全额模型', '差额模型', '比例模型' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [
      {name : '全额模型', type : 'bar', data : data.mx1},
      {name : '差额模型', type : 'bar', data : data.mx2},
      {name : '比例模型', type : 'bar', data : data.mx3}
    ]
  });
  instance.hideLoading();
}

// 刷新图表: 经济损失最小模型
function refresh_jjsszx_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '粮食储备量' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [ {name : '粮食储备量', type : 'bar', data : data.data} ]
  });
  instance.hideLoading();
}

// 刷新图表: 粮食安全模型
function refresh_lsaq_chart(chart_id, data) {
  var instance = echarts.getInstanceByDom(document.getElementById(chart_id));
  instance.setOption({
    tooltip : {trigger : 'axis', axisPointer : {type : 'shadow'}},
    toolbox : {
      show : true,
      orient : 'vertical',
      feature : {
        magicType : {type : [ 'line', 'bar' ]},
        dataView : {show : true},
        restore : {show : true},
        saveAsImage : {show : true},
      }
    },
    legend : {data : [ '粮食储备量' ]},
    grid : {left : '3%', right : '4%', bottom : '3%', containLabel : true},
    xAxis : [ {
      type : 'category',
      axisLabel : {formatter : '{value} 年'},
      data : data.year
    } ],
    yAxis : [ {
      type : 'value',
      axisLabel : {formatter : '{value} 万吨'},
      scale : true
    } ],
    series : [ {name : '粮食储备量', type : 'bar', data : data.data} ]
  });
  instance.hideLoading();
}