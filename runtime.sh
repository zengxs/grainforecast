#!/bin/sh -ex
#
# 安装运行环境

LIBXP6_URL=https://bitbucket.org/zengxs/grainforecast/downloads/libxp6_1.0.2-2_amd64.deb
MCR711_URL=https://bitbucket.org/zengxs/grainforecast/downloads/mcr711.tar.gz

# 本地编译环境
if [ $http_proxy ]; then
    apt_proxy_arg="-o Acquire::http::proxy=$http_proxy"
    curl_proxy_arg="-x $http_proxy"
    cp ./mcr711.tar.gz /tmp/mcr711.tar.gz
else
    # 非本地编译，从 bitbucket 下载 mcr 运行时
    wget $MCR711_URL -o /tmp/mcr711.tar.gz
fi

apt-get $apt_proxy_arg update
apt-get $apt_proxy_arg install locales locales-all -y
update-locale LANG=zh_CN.UTF-8

apt-get $apt_proxy_arg install libxpm4 libxext6 libxt6 libxmu6 -y

curl $LIBXP6_URL -L -o /tmp/libxp6_1.0.2-2_amd64.deb $curl_proxy_arg
dpkg -i /tmp/libxp6_1.0.2-2_amd64.deb

mkdir $MCR_ROOT
tar -xf /tmp/mcr711.tar.gz -C $MCR_ROOT --strip-components=1
