#ifndef __FORECAST_H__
#define __FORECAST_H__

#include <stdio.h> // printf...
#include <uchar.h> // uchar16_t...

#include "libmlprint.h"

#include "libyield.h"
#include "libconsumption.h"
#include "libreserves.h"

// 打印 mxArray，用于调试
void mlprint(mxArray *in)
{
    mxArray *out = NULL;
    mlfMlprint(1, &out, in);
    printf("%s\n", mxArrayToString(out));
}

double
get_double_from_array(double *ptr, size_t index)
{
    return *(ptr + index);
}

/*
创建一个 MATLAB 数组（一维垂直）

将数组 array 中的内容复制到 MATLAB 数组中
返回的 mxArray 使用完成后必须使用 mxDestroyArray 销毁
*/
mxArray *
forecastCreateArray(double *array, size_t len)
{
    mxArray *data = mxCreateDoubleMatrix(len, 1, mxREAL);
    memcpy(mxGetPr(data), array, len * sizeof(double));
    return data;
}

#endif
